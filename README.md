# Watergun

## Lightweight static website editor

![](https://www.kuadrado-software.fr/articles/software/watergun/images/watergun-overview-light.jpg)

**Watergun** is a desktop offline lightweight and cross platform application whose purpose is to easily create a bundle of static web page, deploy it online, and edit its contents at will.

It is developpemed with **NodeJs** and the [**Electron**](https://www.electronjs.org/) API.

This project is at the very beginning of its development so it still lacks a lot of feature and is far to be ready for production.

Watergun is a free and open source project.

**License: GNU GPL v3**

-----------------

## Development - installation of the project

### Prerequisite
- Git
- NodeJS and npm
- Sass

```
$ git clone https://lab.frogg.it/peter_rabbit/watergun-web-editor.git

$ npm install

$ npm run start
```

### Watch style modifications
```
$ npm run style
```
