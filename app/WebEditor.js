const render = require("./generic/render");
const session = require("./generic/session");
const MainActivity = require("./main-activity/main-activity.js");

class WebEditor {
    constructor(domWrapper) {
        this.wrapper = domWrapper;
        this.mainActivity = new MainActivity();
        render.renderCycleRoot = this.mainActivity;
        this.appReady = false;
        this.render();
    }

    initApp() {
        this.initAppTimeout = setTimeout(() => {
            this.mainActivity.initProjectManager();
            this.appReady = true;
            this.render();
            clearTimeout(this.initAppTimeout);
        }, 200);
    }

    render() {
        const display = render.objectToHtml(this.mainActivity.render());

        if (this.wrapper.childNodes.length > 0) {
            this.wrapper.replaceChild(display, this.wrapper.childNodes[0]);
        } else {
            this.wrapper.appendChild(display);
        }

        if (!this.appReady) return this.initApp();

        session.applyGlobalLayoutRules();
    }
}

module.exports = WebEditor;
