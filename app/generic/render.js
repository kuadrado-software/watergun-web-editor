"use strict";

const CUSTOM_TAGS_MAP = {
    document: "div",
    linkBox: "a",
    downloadLink: "a",
    // article: "div",// ...
};

module.exports = {
    objectToHtml: function objectToHtml(obj) {
        const isCustom = Object.keys(CUSTOM_TAGS_MAP).includes(obj.tag);
        const tag = isCustom ? CUSTOM_TAGS_MAP[obj.tag] : obj.tag;

        const node = document.createElement(tag);
        const excludeKeys = ["tag", "contents", "style_rules", "state"];

        Object.keys(obj)
            .filter(attr => !excludeKeys.includes(attr))
            .forEach(attr => {
                if (attr === "class") {
                    node.classList.add(...obj[attr].split(" ").filter(s => s !== ""));
                } else {
                    node[attr] = obj[attr];
                }
            });
        if (obj.contents && typeof obj.contents === "string") {
            node.innerHTML = obj.contents;
        } else {
            obj.contents &&
                obj.contents.length > 0 &&
                obj.contents.forEach(el => {
                    switch (typeof el) {
                        case "string":
                            node.innerHTML = el;
                            break;
                        case "object":
                            node.appendChild(objectToHtml(el));
                            break;
                    }
                });
        }

        if (obj.style_rules) {
            Object.keys(obj.style_rules).forEach(rule => {
                node.style[rule] = obj.style_rules[rule];
            });
        }

        return node;
    },
    renderCycle: function () {
        this.subRender(
            this.renderCycleRoot.render(),
            document.getElementById(this.renderCycleRoot.HTMLLayoutId),
            {
                mode: "replace",
            }
        );
    },
    subRender(object, htmlNode, options = { mode: "append" }) {
        const insert = this.objectToHtml(object);
        switch (options.mode) {
            case "append":
                htmlNode.appendChild(insert);
                break;
            case "override":
                htmlNode.innerHTML = "";
                htmlNode.appendChild(insert);
                break;
            case "insert-before":
                htmlNode.insertBefore(insert, htmlNode.childNodes[options.insertIndex]);
                break;
            case "adjacent":
                /**
                 * options.insertLocation must be one of:
                 *
                 * afterbegin
                 * afterend
                 * beforebegin
                 * beforeend
                 */
                htmlNode.insertAdjacentHTML(options.insertLocation, insert);
                break;
            case "replace":
                htmlNode.parentNode.replaceChild(insert, htmlNode);
                break;
            case "remove":
                htmlNode.remove();
                break;
        }
        document.getElementById("workspace-container").scrollTop = window.workspaceScrollState || 0;
        this.applyGlobalLayoutRules();
    },
    applyGlobalLayoutRules: function () {
        // TODO find a better way to do this
        require("./session").applyGlobalLayoutRules();
    },
};
