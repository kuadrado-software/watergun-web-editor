"use strict";

const fs = require("fs-extra");
const { workspace_dir, empty_project_template } = require("../constants");
const Project = require("./project");
const ProjectDirectory = require("./project-directory");
const session = require("./session");

class ProjectManager {
    constructor() {
        this.createWorkDir();
        this.projectDirectories = this.loadProjectsDirectories();

        if (
            session.states.PROJECT_MANAGER_STATE &&
            Object.keys(session.states.PROJECT_MANAGER_STATE).length > 0
        ) {
            this.recoverSavedState();
        } else {
            session.states.PROJECT_MANAGER_STATE = {};
            this.stateRef = session.states.PROJECT_MANAGER_STATE;
        }
    }

    createWorkDir() {
        if (!fs.existsSync(workspace_dir)) {
            fs.mkdirSync(workspace_dir);
        }
    }

    recoverSavedState() {
        this.stateRef = session.states.PROJECT_MANAGER_STATE;

        if (this.stateRef.loadedProject) {
            const { path, name } = this.stateRef.loadedProject;
            this.loadProject({ path, name }, this.stateRef.loadedProject); // reinstanciate saved project raw object
        }
    }

    createEmptyProject(data) {
        // TODO choose between multiple files or retrieve the existing personnalized template
        const newProjectDir = `${workspace_dir}/${data.name}`;

        try {
            const files = fs.readdirSync(workspace_dir);
            if (files.includes(data.name)) throw new Error("Project is already taken");

            fs.copySync(empty_project_template, newProjectDir);
        } catch (error) {
            console.log(error);
            return;
            // TODO handle error
        }
        this.projectDirectories = this.loadProjectsDirectories();
        this.loadProject({
            path: newProjectDir,
            name: data.name,
            modelTemplatePath: data.modelTemplatePath,
        });
    }

    createEmptyPage(data) {
        this.stateRef.loadedProject.createEmptyPage(data);
    }

    loadProject(data, saved = undefined) {
        const { path, name, modelTemplatePath } = data;
        this.stateRef.loadedProject = new Project({
            name,
            path,
            modelTemplatePath,
            saved,
        });
    }

    loadProjectsDirectories() {
        const files = fs.readdirSync(workspace_dir);
        return files
            .filter(file => fs.statSync(`${workspace_dir}/${file}`).isDirectory())
            .map(file => new ProjectDirectory(file, `${workspace_dir}/${file}`));
    }

    clearLoadedProject() {
        if (this.stateRef.loadedProject) {
            delete this.stateRef.loadedProject;
        }
    }

    selectFile(/*SelectableFile*/ file) {
        this.stateRef.loadedProject.selectFile(file);
    }

    selectFileById(/*float*/ id) {
        this.stateRef.loadedProject.selectFileById(id);
    }

    selectStaticDir(/*SelectableStaticFile []*/ dir) {
        this.stateRef.loadedProject.selectStaticDir(dir);
    }

    deleteStaticFile(/*SelectableStaticFile*/ file) {
        this.stateRef.loadedProject.deleteStaticFile(file);
    }

    renameStaticFile(/*SelectableStaticFile*/ file, newName) {
        this.stateRef.loadedProject.renameStaticFile(file, newName);
    }

    importStaticFiles(/*String[]*/ filePathes, /*String*/ localDirName, autoSelectDir = true) {
        this.stateRef.loadedProject.importStaticFiles(filePathes, localDirName, autoSelectDir);
    }

    selectElement(/*EditableElement*/ element) {
        this.stateRef.loadedProject.selectElement(element);
    }

    deleteElement(/*EditableElement*/ selectedElement) {
        this.stateRef.loadedProject.deleteElement(selectedElement);
    }

    save() {
        this.stateRef.loadedProject.saveAll();
    }

    getLoadedProject() {
        return this.stateRef.loadedProject;
    }

    getSelectedFile() {
        return this.stateRef.loadedProject.state.selectedFile;
    }
}

module.exports = ProjectManager;
