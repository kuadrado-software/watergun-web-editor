"use strict";

const fs = require("fs-extra");
const { FilesTree } = require("./files-tree-utils");
const ImageFile = require("./selectable-file/image-file");
const Page = require("./selectable-file/page");
const SoundFile = require("./selectable-file/sound-file");
const SourceFile = require("./selectable-file/source-file");
const Template = require("./selectable-file/template");
const VideoFile = require("./selectable-file/video-file");
const DocumentFile = require("./selectable-file/document-file");
const { slugify } = require("./utils");

const DEFAULT_MODEL_TEMPLATE_PATH = "generic/root_templates/header-footer";

class Project {
    constructor(data) {
        this.name = data.name;
        this.path = data.path;
        this.modelTemplatePath = data.modelTemplatePath || DEFAULT_MODEL_TEMPLATE_PATH;

        this.filesTree = FilesTree(this.path);

        this.state = {};

        this.initTemplate(data.saved);
        this.initPages(data.saved);
        this.initStatic();
        this.initSelection(data.saved);
    }

    initTemplate(saved) {
        const modelFilePath = `${this.path}/${this.modelTemplatePath}/index.json`;
        const outputDirectory = `${this.path}/template`;
        const retrieveSavedTemplate = () => {
            if (!saved) return false;
            try {
                var templateFile = JSON.parse(
                    fs.readFileSync(`${saved.template.filePath}`, "utf8")
                );
            } catch (error) {
                console.log("Saved template find was not found", error);
                return false;
            }
            this.template = new Template(
                {
                    modelFilePath,
                    outputDirectory,
                },
                {
                    contents: templateFile,
                    name: saved.template.name,
                    filePath: saved.template.filePath,
                }
            );
            return true;
        };

        if (!retrieveSavedTemplate()) {
            this.template = new Template({
                modelFilePath,
                outputDirectory,
            });
        }
    }

    initPages(saved) {
        const pagesDir = this.filesTree.find(dir => dir.name === "pages");

        this.pages = saved
            ? saved.pages.map((p, i) => {
                  return new Page(
                      Object.assign(p, {
                          path: pagesDir.contents[i].path,
                      })
                  );
              })
            : pagesDir.contents.map(p => new Page(p));
    }

    initStatic() {
        const staticDir = this.filesTree.find(dir => dir.name === "static");

        const filesTypes = {
            image: ImageFile,
            video: VideoFile,
            source: SourceFile,
            sound: SoundFile,
            document: DocumentFile,
        };

        this.static = Object.assign(
            staticDir.contents.reduce((dir, subDir) => {
                const key = subDir.name;
                return Object.assign(dir, {
                    [key]: subDir.contents.map(file => new filesTypes[key](file)),
                });
            }, {}),
            {}
        );
    }

    initSelection(saved) {
        if (!saved || (!saved.state.selectedFile && !saved.state.loadedPage)) {
            this.loadPage(this.pages.find(page => page.name === "home-page"));
        } else {
            const savedLoadedPage = this.pages.find(
                page => page.name === saved.state.loadedPage.name
            );
            this.state.loadedPage = savedLoadedPage;

            const savedSelectedFile =
                saved.state.selectedFile.fileType === "template"
                    ? this.template
                    : this.pages.find(page => page.name === saved.state.selectedFile.name);

            this.state.selectedFile = savedSelectedFile;

            const findSelectedElement = (element, root) => {
                if (element.objectId === root.objectId) {
                    return root;
                } else if (Array.isArray(root.contents) && root.contents.length > 0) {
                    let found;
                    for (const child of root.contents) {
                        found = findSelectedElement(element, child);
                        if (found) break;
                    }
                    return found;
                } else {
                    return;
                }
            };

            const savedSelectedElement = saved.state.selectedElement
                ? findSelectedElement(
                      saved.state.selectedElement,
                      this.state.selectedFile.contents
                  ) || this.state.selectedFile.contents
                : this.state.selectedFile.contents;

            this.state.selectedElement = savedSelectedElement;
        }
    }

    loadPage(/*Page*/ page) {
        this.state.loadedPage = page;
        this.state.selectedFile = this.state.loadedPage;
        this.selectElement(this.state.loadedPage.contents);
    }

    selectElement(/*EditableElement*/ el) {
        this.state.selectedElement = el;
    }

    selectFile(/*SelectableFile*/ file) {
        this.state.selectedFile = file;
        this.state.loadedPage = file.fileType === "page" ? file : this.state.loadedPage;
        this.state.selectedElement = this.state.selectedFile.contents;
        delete this.state.selectedStaticDir;
    }

    selectFileById(/*float*/ id) {
        this.selectFile(this.pages.concat([this.template]).find(f => f.id === id));
    }

    selectStaticDir(/*SelectableStaticFile []*/ dir) {
        for (const staticDir of Object.entries(this.static)) {
            const [name, contents] = staticDir;
            if (contents === dir) {
                this.state.selectedStaticDir = { name, contents };
                break;
            }
        }
    }

    deleteStaticFile(/*SelectableStaticFile*/ file) {
        for (const dir of Object.entries(this.static)) {
            const [dirName, dirContents] = dir;
            const found = dirContents.find(f => f === file);
            if (found) {
                try {
                    fs.unlinkSync(found.filePath);
                } catch (error) {
                    console.error(error);
                    // handle error TODO
                    return;
                }
                this.static[dirName].splice(this.static[dirName].indexOf(found), 1);
                return;
            }
        }
    }

    renameStaticFile(/*SelectableStaticFile*/ file, newName) {
        const name = slugify(newName.replace(/\.[a-zA-Z]+/, "")) + "." + file.getFileExt();
        const newPath = file.filePath.replace(file.name, name);
        fs.renameSync(file.filePath, newPath);
        file.filePath = newPath;
        file.name = name;
    }

    importStaticFiles(/*String[]*/ filePathes, /*String*/ localDirName, autoSelectDir) {
        filePathes.forEach(path => {
            const split = path.split("/");
            const filename = split[split.length - 1];
            fs.copyFileSync(
                path,
                `${this.path}/static/${localDirName}/${filename}`,
                fs.constants.COPYFILE_FICLONE
            );
        });
        // refresh project state from actual files
        this.filesTree = FilesTree(this.path);
        this.initStatic();
        autoSelectDir && this.selectStaticDir(this.static[localDirName]);
    }

    updateState(state) {
        Object.assign(this.state, state);
        return this;
    }

    findFile(/*FilesTreeNode*/ file) {
        let found;

        const getSearchTree = tree => {
            return tree.filter(f => f.type === "dir" && f.contents && f.contents.length > 0);
        };

        const find = tree => {
            found = tree.find(f => f === file);
            if (!found) {
                for (const dir of getSearchTree(tree)) {
                    found = find(dir.contents);
                    if (found) break;
                }
            }
            return found;
        };

        return find(this.filesTree);
    }

    saveAll() {
        this.template.save();
        this.pages.forEach(p => p.save());
    }

    createEmptyPage(data) {
        const { slug, contents } = data;
        try {
            fs.writeFileSync(`${this.path}/pages/${slug}.json`, JSON.stringify(contents));
        } catch (error) {
            console.log(error);
            // TODO handle error notif
            return;
        }

        // refresh the pages tree with the new page

        this.filesTree = FilesTree(this.path);

        const pagesDir = this.filesTree.find(dir => dir.name === "pages");

        const freshTreePages = pagesDir.contents
            .map(p => new Page(p))
            .map(page => {
                // keep the currently loaded page unsaved changes
                const findCurrent = this.pages.find(p => p.name === page.name);
                if (findCurrent) {
                    return findCurrent;
                } else return page;
            });

        this.pages = freshTreePages;

        this.loadPage(this.pages.find(p => p.name === slug));
    }

    deleteElement(/*EditableElement*/ selectedElement) {
        const newSelectedElement = this.state.selectedFile.deleteElementAndGetParent(
            selectedElement
        );
        this.selectElement(newSelectedElement);
    }
}

module.exports = Project;
