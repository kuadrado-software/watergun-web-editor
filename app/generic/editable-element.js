"use strict";

const { components_insertion } = require("../constants");
const globalActions = require("./global-actions");

class EditableElement {
    constructor(/*Object*/ obj, /*SelectableFile*/ fileFromId) {
        Object.assign(this, { ...obj });

        this.fileFromId = fileFromId;
        this.objectId = window.performance.now();
        this.genericModel = globalActions.getComponentsManager().getElementGenericModel(this);
        this.protected =
            this.id === "page-container" || this.tag === "main" || this.hasClass("page");

        this.formatChildren();
    }

    formatChildren() {
        if (this.contents && Array.isArray(this.contents) && this.contents.length > 0) {
            this.contents = this.contents.map(childElement => {
                return new EditableElement(childElement, this.fileFromId);
            });
        }
    }

    addClass(className) {
        const currentClass = (this.class || "").split(" ").filter(cl => cl !== "");
        if (!currentClass.includes(className)) {
            currentClass.push(className);
            this.class = currentClass.join(" ");
        }
    }

    removeClass(className) {
        const currentClass = (this.class || "").split(" ").filter(cl => cl !== "");
        if (currentClass.includes(className)) {
            currentClass.splice(currentClass.indexOf(className), 1);
            this.class = currentClass.join(" ");
            this.class === "" && delete this.class;
        }
    }

    hasClass(className) {
        return this.class && this.class.split(" ").includes(className);
    }

    clone() {
        return Object.assign(Object.create(Object.getPrototypeOf(this)), this);
    }

    handleMutationSubmit(form) {
        this.genericModel.handleMutationSubmit.call(this, form);
    }
    getMutationOptions(props) {
        return this.genericModel.mutationOptions.call(this, props);
    }
    getGenericElementType() {
        return this.genericModel.name;
    }
    appendComponent(/*EditableElement*/ newCpt, applyAutoSelect = true) {
        if (!Array.isArray(this.contents)) {
            this.contents = [this.contents];
        }

        this.contents.push(newCpt);

        if (
            applyAutoSelect &&
            components_insertion.auto_select_types
                .map(type => type.name)
                .includes(newCpt.getGenericElementType())
        ) {
            const select = components_insertion.auto_select_types
                .find(type => type.name === newCpt.getGenericElementType())
                .getSelect(newCpt);
            globalActions.getProjectManager().getLoadedProject().selectElement(select);
        }
    }

    update(/*EditableElement*/ obj) {
        Object.assign(this, obj);
        return this;
    }
}

module.exports = EditableElement;
