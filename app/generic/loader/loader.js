"use strict";

class Loader {
    constructor(message) {
        this.message = message;
    }

    render() {
        return {
            tag: "span",
            contents: this.message,
        };
    }
}

module.exports = Loader;
