"use strict";

const globalActions = require("./global-actions");

class Popup {
    constructor(props) {
        this.props = props;
        this.popupId = window.performance.now().toString();
        this.applyShiftRender = false;
    }
    bindClickoutsideListener() {
        this.onClickOutside = this.handleClose.bind(this);
        window.addEventListener("click", this.onClickOutside);
    }

    handleClose(event) {
        if (this.props.preventCloseOnClickOutside) {
            return;
        }
        event.stopImmediatePropagation();
        if (event.target.id === "modal-layer") {
            this.close();
        }
    }

    close() {
        window.removeEventListener("click", this.onClickOutside);
        globalActions.closePopup(this.popupId);
    }

    render() {
        this.bindClickoutsideListener();
    }

    shiftRender() {
        if (!this.applyShiftRender) return;
        const popupContainer = document.getElementById(this.popupId).parentNode;
        const { x, y } = popupContainer.getBoundingClientRect();
        popupContainer.style.left = (Math.round(x) + 20).toString() + "px";
        popupContainer.style.top = (Math.round(y) - 20).toString() + "px";
    }
}

module.exports = Popup;
