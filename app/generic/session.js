"use strict";

const render = require("./render");

module.exports = {
    /**************************************************
    Components will write any persistent states here, 
    the the object will be saved in the save.json file 
    and app will try to load it on starting.
    **************************************************/
    states: {
        globalLayoutState: [
            {
                nodeId: "explorer",
                rules: {
                    width: "auto",
                },
            },
            {
                nodeId: "toolbar",
                rules: {
                    width: "300px",
                },
            },
        ],
        // Rendered components will write persistent states here
    },
    writeState: function (obj, state) {
        /* The obj parameter will be a reference to a state 
        defined in this.states by any component that needs it */
        Object.assign(obj, state);
        render.renderCycle();
    },
    writeGlobalLayoutState: function (id, state) {
        const { globalLayoutState } = this.states;
        const writeNode = globalLayoutState.find(node => node.nodeId === id);
        Object.assign(writeNode.rules, state);
    },
    applyGlobalLayoutRules: function () {
        this.states.globalLayoutState.forEach(node => {
            const element = document.getElementById(node.nodeId);
            Object.keys(node.rules).forEach(rule => {
                element.style[rule] = node.rules[rule];
            });
        });
    },
};
