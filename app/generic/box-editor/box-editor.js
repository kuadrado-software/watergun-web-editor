"use strict";

const EditableElement = require("../editable-element");
const globalActions = require("../global-actions");
const BoxInsertElementSelector = require("../inputs/box-insert-element-selector");
const render = require("../render");
const { getFormFields } = require("../utils");

class BoxEditor {
    constructor(props) {
        this.props = props;
        this.state = {
            box: new EditableElement({ ...this.props.box }, this.props.box.fileFromId),
            dragndrop: {
                indexStart: -1,
                indexDrop: -1,
            },
        };
        this.id = window.performance.now().toString();
        this.resetInsertElementSelector();
        this.initSelection();
    }

    resetInsertElementSelector() {
        this.elementSelector = new BoxInsertElementSelector({
            preventRefresh: true,
            onChange: this.handleSelectInsertElementChange.bind(this),
        });
    }

    initSelection() {
        this.state.selection = this.state.box; // Select box root by default
    }

    handleSubmitInsertComponent(e) {
        e.preventDefault();
        const { box } = this.state;

        const template = getFormFields(e.target)
            .find(f => f.name === "element-selector")
            .value.template();

        const newCpt = new EditableElement(template, box.fileFromId);

        newCpt.handleMutationSubmit(e.target);

        box.appendComponent(newCpt, false);
        this.resetInsertElementSelector();

        this.state.selection = newCpt;
        this.refresh();
    }

    handleSelectInsertElementChange(e) {
        this.refresh("insert-form");
    }

    handleElementClick(element) {
        this.state.selection = element;
        this.refresh();
    }

    refresh(root = "box-editor") {
        const rootNode = document.getElementById(this.id);
        let renderObj, renderNode;
        switch (root) {
            case "box-editor":
                renderObj = this.render();
                renderNode = rootNode;
                break;
            case "overview":
                renderObj = this.renderBoxOverview();
                renderNode = rootNode.getElementsByClassName("overview")[0];
                break;
            case "insert-form":
                renderObj = this.renderInsertForm();
                renderNode = rootNode.getElementsByTagName("form")[0];
                break;
            case "components-tree":
                renderObj = this.renderComponentsTree();
                renderNode = rootNode.getElementsByClassName("components-tree")[0];
                break;
            case "edit-selection":
                renderObj = this.renderSelectedElementMutationOptions();
                renderNode = rootNode.getElementsByClassName("edit-selection")[0];
                break;
        }

        renderObj && renderNode && render.subRender(renderObj, renderNode, { mode: "replace" });
    }

    applyDragndrop() {
        const { indexStart, indexDrop } = this.state.dragndrop;
        const { contents } = this.state.box;

        const destinationIndex =
            indexDrop < indexStart ? indexDrop : indexDrop > indexStart + 1 ? indexDrop - 1 : -1;

        const shiftList = () => {
            if (destinationIndex > -1) {
                const tmp = contents[indexStart];
                contents.splice(indexStart, 1);
                contents.splice(destinationIndex, 0, tmp);
                this.refresh();
            }
        };

        shiftList();
    }

    handleDeleteSelectedElement() {
        const tmp = this.state.selection;
        this.state.selection = this.state.box;
        this.state.box.contents.splice(this.state.box.contents.indexOf(tmp), 1);
        this.refresh();
    }

    renderBoxOverview() {
        return {
            tag: "div",
            class: `overview ${this.state.selection === this.state.box ? "editor-selection" : ""}`,
            onclick: this.handleElementClick.bind(this, this.state.box),
            contents: [
                {
                    tag: "div",
                    class: "overview-header",
                    contents: [{ tag: "span", contents: "Box" }],
                },
                this.state.box,
            ],
        };
    }

    renderInsertForm() {
        const selectedValue = this.elementSelector.getValue();
        return {
            tag: "form",
            onsubmit: this.handleSubmitInsertComponent.bind(this),
            contents: [this.elementSelector.render()].concat(
                selectedValue
                    ? [
                          {
                              tag: "button",
                              class: "gui-button discrete",
                              type: "submit",
                              contents: `Insert component <i>${selectedValue.name}</i>`,
                          },
                      ]
                    : []
            ),
        };
    }

    renderComponentsTree() {
        return {
            tag: "div",
            class: "components-tree",
            contents: [
                {
                    tag: "div",
                    contents: [
                        {
                            tag: "span",
                            contents: "Manage box contents",
                            title: "Drag & drop to reorganize components or delete them",
                        },
                    ],
                },
                {
                    tag: "ul",
                    contents: this.state.box.contents
                        .map((component, i) => {
                            return [
                                {
                                    tag: "li",
                                    class: "drop-placeholder",
                                    ondragover: e => {
                                        e.preventDefault();
                                        Array.from(
                                            document
                                                .getElementById(this.id)
                                                .getElementsByClassName("drop-placeholder")
                                        ).forEach(li => {
                                            if (e.target !== li) {
                                                li.classList.remove("hovered");
                                            } else {
                                                li.classList.add("hovered");
                                                this.state.dragndrop.indexDrop = i;
                                            }
                                        });
                                    },
                                },
                                {
                                    tag: "li",
                                    class: `draggable ${
                                        this.state.selection === component ? "editor-selection" : ""
                                    }`,
                                    onclick: this.handleElementClick.bind(this, component),
                                    draggable: true,
                                    ondragstart: e => {
                                        this.state.dragndrop.indexStart = i;
                                        const findLiNode = el => {
                                            if (el.classList.contains("draggable")) {
                                                return el;
                                            } else {
                                                return findLiNode(el.parentNode);
                                            }
                                        };
                                        const liNode = findLiNode(e.target);
                                        liNode.classList.add("moved");
                                    },
                                    ondragend: e => {
                                        e.preventDefault();
                                        Array.from(
                                            document
                                                .getElementById(this.id)
                                                .getElementsByClassName("drop-placeholder")
                                        ).forEach(li => {
                                            li.classList.remove("hovered");
                                        });
                                        document
                                            .getElementById(this.id)
                                            .getElementsByClassName("moved")[0]
                                            .classList.remove("moved");
                                        this.applyDragndrop();
                                    },
                                    contents: [
                                        {
                                            tag: "span",
                                            class: "overview-header",
                                            contents: globalActions
                                                .getComponentsManager()
                                                .getElementGenericModel(component).name,
                                        },
                                        {
                                            tag: "div",
                                            class: "component-overview",
                                            contents: [component],
                                        },
                                    ],
                                },
                            ].concat(
                                i === this.state.box.contents.length - 1
                                    ? [
                                          {
                                              tag: "li",
                                              class: "drop-placeholder",
                                              ondragover: e => {
                                                  e.preventDefault();
                                                  Array.from(
                                                      document
                                                          .getElementById(this.id)
                                                          .getElementsByClassName(
                                                              "drop-placeholder"
                                                          )
                                                  ).forEach(li => {
                                                      if (e.target !== li) {
                                                          li.classList.remove("hovered");
                                                      } else {
                                                          li.classList.add("hovered");
                                                          this.state.dragndrop.indexDrop = i + 1;
                                                      }
                                                  });
                                              },
                                          },
                                      ]
                                    : []
                            );
                        })
                        .flat(),
                },
                this.renderInsertForm(),
            ],
        };
    }

    renderDeleteButton() {
        if (this.state.selection === this.state.box || this.state.selection.protected) return {};
        return {
            tag: "div",
            class: "delete-btn",
            contents: [
                {
                    tag: "button",
                    class: "gui-button",
                    contents: `Delete component <i>${
                        globalActions
                            .getComponentsManager()
                            .getElementGenericModel(this.state.selection).name
                    }</i>`,
                    onclick: this.handleDeleteSelectedElement.bind(this),
                },
            ],
        };
    }

    renderSelectedElementMutationOptions() {
        const formId = window.performance.now().toString();

        const renderApplyBtn =
            globalActions.getComponentsManager().getElementGenericModel(this.state.selection)
                .applyMutationOption === "button";

        const onsubmit = e => {
            e.preventDefault();
            applySubmit();
        };

        const applySubmit = () => {
            try {
                this.state.selection.handleMutationSubmit(document.getElementById(formId));
            } catch (error) {
                console.log(error);
                alert("Enter a value");
                return;
            }
            this.refresh();
        };

        return {
            tag: "div",
            class: "edit-selection",
            contents: [
                {
                    tag: "span",
                    contents: "Edit selection",
                },
                {
                    tag: "form",
                    class: "mutation-form",
                    id: formId,
                    onsubmit,
                    contents: this.state.selection
                        .getMutationOptions({
                            styleOnly: this.state.selection === this.state.box,
                            triggerSubmit: applySubmit,
                        })
                        .concat(
                            renderApplyBtn
                                ? [
                                      {
                                          tag: "input",
                                          type: "submit",
                                          value: "Apply",
                                          class: "gui-button",
                                      },
                                  ]
                                : []
                        ),
                },
                this.renderDeleteButton(),
            ],
        };
    }

    renderButtons() {
        return {
            tag: "div",
            class: "buttons",
            contents: [
                {
                    tag: "button",
                    class: "gui-button discrete",
                    type: "button",
                    contents: "Save disposition as a reusable component",
                    onclick: e => console.log("save disposition", this.state.box),
                },
                {
                    tag: "button",
                    class: "gui-button",
                    type: "button",
                    contents: "Apply changes",
                    onclick: () => this.props.onSubmit(this.state.box),
                },
            ],
        };
    }

    render() {
        return {
            tag: "div",
            class: "box-editor",
            id: this.id,
            contents: [
                this.renderBoxOverview(),
                this.renderComponentsTree(),
                this.renderSelectedElementMutationOptions(),
                this.renderButtons(),
            ],
        };
    }
}

module.exports = BoxEditor;
