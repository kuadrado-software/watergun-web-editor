"use strict";

const globalActions = require("../global-actions");
const render = require("../render");

class BoxInsertElementSelector {
    constructor(props) {
        this.props = props;
        const UNSUPPORTED_ELEMENTS = ["listItem", "source"];
        const defaultComponents = globalActions.getComponentsManager().defaultComponents;
        this.elements = Object.entries(defaultComponents)
            .filter(kv => !UNSUPPORTED_ELEMENTS.includes(kv[0]))
            .map(kv => {
                return { [kv[0]]: kv[1] };
            })
            .reduce((obj, entry) => {
                return Object.assign(obj, entry);
            });
        this.id = window.performance.now().toString();
        this.state = {
            dataBinderForm: [],
        };
    }

    reset() {
        Object.keys(this.state).forEach(k => delete this.state[k]);
        this.state.dataBinderForm = [];
        delete this.state.selectedOption;
    }

    handleSelectChange(e) {
        this.reset();
        this.state.selectedOption = this.elements[e.target.value]();
        !this.props.preventRefresh &&
            render.subRender(this.render(), document.getElementById(this.id), { mode: "replace" });
        this.props.onChange && this.props.onChange(e);
    }

    getDataBinderForm() {
        if (
            this.state.selectedOption &&
            this.state.selectedOption.bindDataInputs &&
            this.state.selectedOption.name !== "Box"
        ) {
            this.state.dataBinderForm = this.state.selectedOption.bindDataInputs();
        } else this.state.dataBinderForm = [];

        return this.state.dataBinderForm;
    }

    getValue() {
        return this.state.selectedOption;
    }

    render() {
        return {
            tag: "div",
            id: this.id,
            class: "element-selector",
            name: "element-selector",
            value: this.getValue.bind(this),
            contents: [
                {
                    tag: "div",
                    class: "input-label",
                    contents: [
                        {
                            tag: "span",
                            contents: "Insert a component",
                        },
                    ],
                },
                {
                    tag: "select",
                    name: "insert-component",
                    onchange: this.handleSelectChange.bind(this),
                    contents: [
                        {
                            tag: "option",
                            contents: "Choose a component",
                            disabled: true,
                            selected: !this.state.selectedOption,
                        },
                    ].concat(
                        Object.entries(this.elements).map(kv => {
                            const [key, get] = kv;
                            const { name } = get();
                            return {
                                tag: "option",
                                value: key,
                                contents: name,
                                selected:
                                    this.state.selectedOption &&
                                    this.state.selectedOption.name === get().name,
                            };
                        })
                    ),
                },
            ].concat(this.getDataBinderForm()),
        };
    }
}

module.exports = BoxInsertElementSelector;
