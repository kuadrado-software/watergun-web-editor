"use strict";

const StaticFilePickerPopup = require("../../main-activity/components/popups/static-file-picker-popup/static-file-picker-popup");
const globalActions = require("../global-actions");
const render = require("../render");

class StaticFileInput {
    constructor(props) {
        this.props = props;
        this.state = {};
        if (this.props.editInstance) {
            this.state.file = this.props.editInstance;
        }
        this.id = window.performance.now().toString();
        this.filePickerPopup = new StaticFilePickerPopup(
            Object.assign(this.props, {
                onSelectFile: this.handleSelectFile.bind(this),
                preventCloseOnClickOutside: true,
            })
        );
    }

    getValue() {
        return this.state.file ? this.state.file.filePath : undefined;
    }

    handleSelectFile(file) {
        this.state.file = file;
        globalActions.closePopup(this.filePickerPopup.popupId);

        render.subRender(
            this.renderFileOverview(),
            document
                .getElementById(this.id)
                .getElementsByClassName("static-file-input-overview")[0],
            { mode: "replace" }
        );
    }

    handleShowFilePicker(e) {
        e.preventDefault();
        globalActions.showPopup(this.filePickerPopup);
    }

    getFileOverView() {
        if (this.props.editInstance && this.state.file === this.props.editInstance) {
            return this.state.file;
        } else {
            return this.state.file.getDisplayTemplate();
        }
    }

    renderFileOverview() {
        return {
            tag: "div",
            class: "static-file-input-overview",
            contents: this.state.file ? [this.getFileOverView()] : [],
        };
    }

    render() {
        return {
            tag: "div",
            id: this.id,
            class: "static-file-input",
            name: this.props.name || "src",
            contents: [
                this.renderFileOverview(),
                {
                    tag: "button",
                    class: "gui-button discrete",
                    type: "button",
                    contents: this.props.buttonText || "Choose a file",
                    onclick: this.handleShowFilePicker.bind(this),
                },
            ],
            value: this.getValue.bind(this),
        };
    }
}

module.exports = StaticFileInput;
