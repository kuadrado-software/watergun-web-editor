"use strict";

const BoxEditorPopup = require("../../main-activity/components/popups/box-editor-popup/box-editor-popup");
const EditableElement = require("../editable-element");
const globalActions = require("../global-actions");

class BoxInput {
    constructor(props) {
        this.props = props;
        this.state = {
            box:
                this.props.editInstance ||
                new EditableElement(
                    globalActions.getComponentsManager().defaultCompoents.box().template()
                ),
        };
        this.editorPopup = new BoxEditorPopup({
            box: this.state.box,
            onSubmit: this.handleSubmitBoxEditor.bind(this),
            preventCloseOnClickOutside: true,
        });
    }

    getValue() {
        return this.state.box;
    }

    handleSubmitBoxEditor(box) {
        this.state.box = box;
        globalActions.closePopup(this.editorPopup.popupId);
        this.props.triggerSubmit(); // avoid having to click "apply" twice.
    }

    handleEditClick(e) {
        globalActions.showPopup(this.editorPopup);
    }

    render() {
        const { styleOnly } = this.props;
        return {
            tag: "div",
            contents: [
                { tag: "div", contents: [{ tag: "span", contents: "Styling options" }] }, // TMP
            ].concat(
                styleOnly
                    ? []
                    : [
                          {
                              tag: "button",
                              type: "button",
                              class: "gui-button discrete",
                              contents: "Open box editor",
                              onclick: this.handleEditClick.bind(this),
                          },
                      ]
            ),
            class: "box-input",
            name: "box",
            value: this.getValue.bind(this),
        };
    }
}

module.exports = BoxInput;
