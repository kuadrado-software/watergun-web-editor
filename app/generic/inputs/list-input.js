"use strict";

const EditableElement = require("../editable-element");
const globalActions = require("../global-actions");
const render = require("../render");

class ListInput {
    constructor(/*EditableElement*/ props) {
        this.props = props || {};
        this.id = window.performance.now().toString();
        const { editInstance } = this.props;
        const UNSUPPORTED_LISTITEMS_TYPES = ["box", "source", "listItem", "list"];
        this.defaultComponents = Object.entries(
            globalActions.getComponentsManager().defaultComponents
        )
            .filter(kv => !UNSUPPORTED_LISTITEMS_TYPES.includes(kv[0]))
            .map(kv => {
                const [tag, cpt] = kv;
                return { [tag]: cpt };
            })
            .reduce((obj, entry) => {
                return Object.assign(obj, entry);
            }, {});

        this.state = {
            listItems:
                editInstance && editInstance.contents && editInstance.contents.length > 0
                    ? editInstance.contents
                    : [],
            listType: this.getListType(editInstance),
            newListItemForm: {
                show: false,
            },
        };
    }

    getListType(editInstance) {
        if (!editInstance || !editInstance.contents || editInstance.contents.length === 0)
            return "";

        for (const obj of editInstance.contents) {
            if (!obj.contents || obj.contents.length === 0) continue;
            for (const o of obj.contents) {
                const type = o.tag;
                if (type) return type;
            }
        }
        return "";
    }

    getValue() {
        return this.state.listItems;
    }

    handleListTypeChange(e) {
        this.state.listType = e.target.value;
        this.state.newListItemForm.show = false;
        delete this.state.newListItemForm.inputs;
        this.refreshListItemsForm();
    }

    handleAddItemButtonClick(e) {
        e.preventDefault();

        const listItemModel = globalActions
            .getComponentsManager()
            .getTagGenericModel(this.state.listType);

        const listItemTypeInputs = listItemModel.bindDataInputs();

        this.state.newListItemForm.show = true;
        this.state.newListItemForm.inputs = listItemTypeInputs.contents.concat(
            listItemModel.applyMutationOption === "button"
                ? [
                      {
                          tag: "button",
                          class: "gui-button discrete",
                          contents: "Insert list item",
                          type: "submit",
                      },
                  ]
                : []
        );

        this.refreshListItemsForm();
    }

    handleAddItemFormSubmit(e) {
        e.preventDefault();
        e.stopImmediatePropagation();

        const model = globalActions.getComponentsManager().getTagGenericModel(this.state.listType);

        const newLi = model.template();
        model.handleMutationSubmit(document.getElementById("add-item-form"), newLi);

        this.state.listItems.push(
            new EditableElement(
                { tag: "li", contents: [newLi] },
                globalActions.getProjectManager().getSelectedFile().id
            )
        );
        this.state.newListItemForm.show = false;
        delete this.state.newListItemForm.inputs;

        this.refreshListItemsForm();
    }

    refreshListItemsForm() {
        const renderNode = document.getElementById(this.id);
        render.subRender(this.render(), renderNode, {
            mode: "replace",
        });
    }

    renderEditListItems() {
        const { listItems, listType, newListItemForm } = this.state;
        return {
            tag: "ul",
            id: "edit-list-items",
            contents: listItems
                // show the current list items
                .map(li =>
                    Object.assign(li, {
                        class: "box-overview",
                        style_rules: { margin: "5px 0" },
                    })
                )
                // concat a form to add more items
                .concat([
                    {
                        tag: "li",
                        style_rules: { listStyleType: "none" },
                        contents: [
                            {
                                tag: "form",
                                id: "add-item-form",
                                onsubmit: this.handleAddItemFormSubmit.bind(this),
                                contents: (
                                    newListItemForm.inputs ||
                                    (listItems.length === 0
                                        ? [{ tag: "span", contents: "Empty" }]
                                        : [])
                                ).concat(
                                    listType && !newListItemForm.show
                                        ? [
                                              {
                                                  tag: "input",
                                                  type: "submit",
                                                  class: "gui-button discrete",
                                                  value: "+",
                                                  title: "Add a list item",
                                                  onclick: this.handleAddItemButtonClick.bind(this),
                                              },
                                          ]
                                        : []
                                ),
                            },
                        ],
                    },
                ]),
        };
    }

    renderListTypeSelector() {
        return {
            tag: "select",
            id: "list-type-selector",
            reserved: true,
            onchange: this.handleListTypeChange.bind(this),
            contents: [
                {
                    tag: "option",
                    selected: this.state.listType === "",
                    disabled: true,
                    contents: "Choose a list item component type",
                },
            ].concat(
                Object.values(this.defaultComponents).map(type => {
                    const { name, template } = type();
                    const tag = template().tag;
                    return {
                        tag: "option",
                        contents: name,
                        value: tag,
                        selected: this.state.listType === tag,
                    };
                })
            ),
        };
    }

    renderInputs() {
        return [
            {
                tag: "div",
                id: "inputs-container",
                contents: (this.state.listType === "" || this.state.listItems.length === 0
                    ? [
                          // if list doesn't have a type yet, display a type selector,
                          {
                              tag: "label",
                              contents: "Type of the list",
                          },
                          this.renderListTypeSelector(),
                      ]
                    : [
                          {
                              // else just indicate which type the list is
                              tag: "div",
                              contents: [
                                  {
                                      tag: "span",
                                      contents: `List type : ${
                                          globalActions
                                              .getComponentsManager()
                                              .getTagGenericModel(this.state.listType).name
                                      }`,
                                  },
                              ],
                          },
                      ]
                ).concat([
                    {
                        tag: "div",
                        contents: [
                            {
                                tag: "span",
                                contents: "List contents :",
                            },
                            this.renderEditListItems(),
                        ],
                    },
                ]),
            },
        ];
    }

    render() {
        return {
            tag: "div",
            class: "list-input",
            id: this.id,
            name: "contents",
            value: this.getValue.bind(this), // dynamic value
            contents: this.renderInputs(),
        };
    }
}

module.exports = ListInput;
