"use strict";

const genericImplementation = require("./generic-implementation");

module.exports = function paragraph() {
    return Object.assign(genericImplementation(paragraph), {
        name: "Paragraph",
        template() {
            return { tag: "p", contents: "" };
        },
        bindDataInputs(arg = {}) {
            const { editInstance } = arg;
            return {
                tag: "div",
                class: "data-binder-inputs-container",
                contents: [
                    {
                        tag: "div",
                        class: "input-block",
                        contents: [
                            {
                                tag: "textarea",
                                required: true,
                                contents: editInstance
                                    ? editInstance.contents
                                          .replaceAll("<br />", "\n")
                                          .replaceAll("&nbsp;", " ")
                                    : "",
                                placeholder: "Type your text here",
                                autofocus: true,
                                name: "contents",
                            },
                        ],
                    },
                ],
            };
        },
    });
};
