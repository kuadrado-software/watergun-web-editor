"use strict";

const genericImplementation = require("./generic-implementation");

module.exports = function inlineText() {
    return Object.assign(genericImplementation(inlineText), {
        name: "Inline text",
        template() {
            return { tag: "span", contents: "" };
        },
        applyMutationOption: "auto",
    });
};
