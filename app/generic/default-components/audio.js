"use strict";

const StaticFileInput = require("../inputs/static-file-input");
const { getFormFields, getFileExt } = require("../utils");
const genericImplementation = require("./generic-implementation");

module.exports = function audio() {
    return Object.assign(genericImplementation(audio), {
        name: "Sound",
        template() {
            return { tag: "audio", controls: true, contents: [{ tag: "source" }] };
        },
        bindDataInputs(arg = {}) {
            const { editInstance } = arg;
            return {
                tag: "div",
                class: "data-binder-inputs-container",
                contents: [
                    {
                        tag: "div",
                        class: "input-block",
                        contents: [
                            new StaticFileInput({ staticDir: "sound", editInstance }).render(),
                        ],
                    },
                ],
            };
        },
        handleMutationSubmit(form, externalObject = undefined) {
            const applyTo = externalObject || this;
            // apply inputs to the audio inner source element
            const sourceEl = applyTo.contents.find(el => el.tag === "source");
            getFormFields(form).forEach(f => {
                sourceEl[f.name] = f.value;
            });
            sourceEl.type = `audio/${getFileExt(sourceEl.src || "")}`;
        },
    });
};
