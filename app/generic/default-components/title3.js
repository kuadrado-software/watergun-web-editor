"use strict";

const genericImplementation = require("./generic-implementation");

module.exports = function title3() {
    return Object.assign(genericImplementation(title3), {
        name: "Title 3",
        template() {
            return { tag: "h3", contents: "" };
        },
        applyMutationOption: "auto",
    });
};
