"use strict";

const genericImplementation = require("./generic-implementation");

module.exports = function title4() {
    return Object.assign(genericImplementation(title4), {
        name: "Title 4",
        template() {
            return { tag: "h4", contents: "" };
        },
        applyMutationOption: "auto",
    });
};
