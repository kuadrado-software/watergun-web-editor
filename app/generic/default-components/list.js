"use strict";

const ListInput = require("../inputs/list-input");
const genericImplementation = require("./generic-implementation");

module.exports = function list() {
    return Object.assign(genericImplementation(list), {
        name: "List",
        template() {
            return {
                tag: "ul",
            };
        },
        bindDataInputs(arg = {}) {
            const { editInstance } = arg;

            return {
                tag: "div",
                class: "data-binder-inputs-container",
                contents: [
                    {
                        tag: "div",
                        class: "input-block",
                        contents: [
                            new ListInput({
                                editInstance
                            }).render(),
                        ],
                    },
                ],
            };
        },
    });
};
