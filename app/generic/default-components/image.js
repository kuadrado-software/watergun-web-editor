"use strict";

const StaticFileInput = require("../inputs/static-file-input");
const genericImplementation = require("./generic-implementation");

module.exports = function image() {
    return Object.assign(genericImplementation(image), {
        name: "Image",
        template() {
            return { tag: "img" };
        },
        bindDataInputs(arg = {}) {
            const { editInstance } = arg;
            return {
                tag: "div",
                class: "data-binder-inputs-container",
                contents: [
                    {
                        tag: "div",
                        class: "input-block",
                        contents: [
                            new StaticFileInput({ staticDir: "image", editInstance }).render(),
                        ],
                    },
                ],
            };
        },
    });
};
