"use strict";

const BoxInput = require("../inputs/box-input");
const { getFormFields } = require("../utils");
const genericImplementation = require("./generic-implementation");

module.exports = function box() {
    return Object.assign(genericImplementation(box), {
        name: "Box",
        template() {
            return {
                tag: "div",
                contents: [],
            };
        },
        bindDataInputs(arg = {}) {
            const { editInstance, triggerSubmit, styleOnly } = arg;
            return {
                tag: "div",
                class: "data-binder-inputs-container",
                contents: editInstance
                    ? [
                          {
                              tag: "div",
                              class: "box-overview",
                              contents:
                                  editInstance.contents.length > 0
                                      ? [editInstance]
                                      : [
                                            Object.assign(
                                                { ...editInstance },
                                                {
                                                    contents: [
                                                        {
                                                            tag: "span",
                                                            contents: "Empty box",
                                                            style_rules: { fontStyle: "italic" },
                                                        },
                                                    ],
                                                }
                                            ),
                                        ],
                          },
                          new BoxInput({ editInstance, triggerSubmit, styleOnly }).render(),
                      ]
                    : [new BoxInput({ triggerSubmit, styleOnly }).render()],
            };
        },
        handleMutationSubmit(form, /*EditableElement*/ externalObject = undefined) {
            getFormFields(form).forEach(input => {
                if (input.name === "box") {
                    const applyTo = externalObject || this;
                    applyTo.update(input.value);
                }
            });
        },
        applyMutationOption: "auto",
    });
};
