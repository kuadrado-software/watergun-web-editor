"use strict";

const genericImplementation = require("./generic-implementation");

module.exports = function button() {
    return Object.assign(genericImplementation(button), {
        name: "button",
        template() {
            return {
                tag: "button",
            };
        },
        bindDataInputs(arg = {}) {
            const { editInstance = {} } = arg;
            const { onclick = "", contents = "" } = editInstance;
            return {
                tag: "div",
                class: "data-binder-inputs-container",
                contents: [
                    {
                        tag: "div",
                        class: "input-block",
                        contents: [
                            {
                                tag: "label",
                                for: "onclick",
                                contents: "Action (TODO)",
                            },
                            {
                                tag: "input",
                                type: "text",
                                name: "onclick",
                                defaultValue: onclick,
                                disabled: true,
                            },
                        ],
                    },
                    {
                        tag: "div",
                        class: "input-block",
                        contents: [
                            {
                                tag: "label",
                                for: "text",
                                contents: "Button text",
                            },
                            {
                                tag: "input",
                                type: "text",
                                name: "contents",
                                defaultValue: contents,
                                placeholder: "Click me !",
                                required: true,
                            },
                        ],
                    },
                ],
            };
        },
    });
};
