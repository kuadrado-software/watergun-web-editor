"use strict";

const genericImplementation = require("./generic-implementation");

module.exports = function pageContainer() {
    return Object.assign(genericImplementation(pageContainer), {
        name: "Page container",
        template() {
            return { tag: "div", id: "page-container" };
        },
        bindDataInputs() {
            return {
                tag: "div",
                class: "data-binder-inputs-container",
                contents: "page container mutation options (styling only)",
            };
        },
        handleMutationSubmit(form) {
            console.log("handle page container modif");
        },
    });
};
