"use strict";

const link = require("./link");
const button = require("./button");
const list = require("./list");
const inlineText = require("./inline-text");
const paragraph = require("./paragraph");
const title1 = require("./title1");
const title2 = require("./title2");
const title3 = require("./title3");
const title4 = require("./title4");
const image = require("./image");
const listItem = require("./list-item");
const audio = require("./audio");
const video = require("./video");
const linkBox = require("./link-box");
const box = require("./box");
const pageContainer = require("./page-container");
const downloadLink = require("./download-link");

const DEFAULT_COMPONENTS = {
    box,
    title1,
    title2,
    title3,
    title4,
    paragraph,
    inlineText,

    link,
    linkBox, // WIP
    downloadLink,
    list,
    listItem,

    button,
    image,
    audio,
    source: () => {
        return {
            name: "Source",
            template() {
                return { tag: "source" };
            },
        };
    },
    video,
    // iframe: "iframe",
    // canvas: "canvas",
};

const PAGE_CONTAINER_MODEL = pageContainer;

module.exports = { DEFAULT_COMPONENTS, PAGE_CONTAINER_MODEL };
