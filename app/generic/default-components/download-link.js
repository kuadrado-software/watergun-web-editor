"use strict";

const EditableElement = require("../editable-element");
const globalActions = require("../global-actions");
const StaticFileInput = require("../inputs/static-file-input");
const render = require("../render");
const { getFormFields } = require("../utils");
const genericImplementation = require("./generic-implementation");

class DownloadLinkInputs {
    constructor(props) {
        this.props = props;

        this.state = {
            showImagePicker:
                this.props.editInstance &&
                this.props.editInstance.contents.find(el => el.tag === "img")
                    ? true
                    : false,
        };
        this.id = window.performance.now().toString();
    }

    renderLinkImageInput() {
        const { editInstance } = this.props;
        return {
            tag: "div",
            class: "link-image-input",
            contents: [
                {
                    tag: "div",
                    class: "input-block inline-block",
                    contents: [
                        { tag: "label", contents: "Set an image for the link" },
                        {
                            tag: "input",
                            type: "checkbox",
                            name: "includeLinkImage",
                            value: this.state.showImagePicker,
                            onchange: () => {
                                this.state.showImagePicker = !this.state.showImagePicker;
                                render.subRender(
                                    this.renderLinkImageInput(),
                                    document
                                        .getElementById(this.id)
                                        .getElementsByClassName("link-image-input")[0],
                                    { mode: "replace" }
                                );
                            },
                            checked: this.state.showImagePicker,
                        },
                    ],
                },
                {
                    tag: "div",
                    class: "input-block",
                    contents: this.state.showImagePicker
                        ? [
                              new StaticFileInput({
                                  staticDir: "image",
                                  editInstance: editInstance
                                      ? editInstance.contents.find(el => el.tag === "img")
                                      : undefined,
                                  name: "src",
                                  buttonText:
                                      editInstance &&
                                      editInstance.contents.find(el => el.tag === "img")
                                          ? "Change link image"
                                          : "Choose an image for the link",
                              }).render(),
                          ]
                        : [],
                },
            ],
        };
    }

    render() {
        const { editInstance } = this.props;
        return {
            tag: "div",
            id: this.id,
            contents: [
                {
                    tag: "div",

                    class: "input-block",
                    contents: [
                        new StaticFileInput({
                            staticDir: "*",
                            editInstance,
                            name: "href",
                            buttonText: editInstance ? "Change file" : "Choose a file to download",
                        }).render(),
                    ],
                },
                {
                    tag: "div",
                    class: "input-block",
                    contents: [
                        {
                            tag: "label",
                            for: "linkText",
                            contents: "Text",
                        },
                        {
                            tag: "input",
                            type: "text",
                            name: "linkText",
                            required: true,
                            placeholder: "Enter your text here",
                            defaultValue: editInstance
                                ? editInstance.contents
                                      .find(el => el.tag === "span")
                                      .contents.replaceAll("&nbsp;", " ")
                                : "",
                        },
                    ],
                },
                this.renderLinkImageInput(),
            ],
        };
    }
}

module.exports = function downloadLink() {
    return Object.assign(genericImplementation(downloadLink), {
        name: "Download link",
        template() {
            return {
                tag: "downloadLink",
                download: true,
                contents: [{ tag: "span", contents: "salut" }],
            };
        },
        bindDataInputs(arg = {}) {
            const { editInstance } = arg;
            return {
                tag: "div",
                class: "data-binder-inputs-container",
                contents: [new DownloadLinkInputs({ editInstance }).render()],
            };
        },
        handleMutationSubmit(form) {
            getFormFields(form).forEach(f => {
                switch (f.name) {
                    case "src":
                        let imgEl = this.contents.find(el => el.tag === "img");
                        if (!imgEl) {
                            imgEl = new EditableElement(
                                { tag: "img", style_rules: { display: "block" } },
                                globalActions.getProjectManager().getSelectedFile().id
                            );
                            this.contents.unshift(imgEl);
                        }
                        imgEl.src = f.value;
                        break;
                    case "href":
                        this.href = f.value;
                        break;
                    case "linkText":
                        this.contents.find(el => el.tag === "span").contents = f.value;
                        break;
                    case "includeLinkImage":
                        if (f.value === "false") {
                            const imgIndex = this.contents.indexOf(
                                this.contents.find(el => el.tag === "img")
                            );
                            const hasContentsImg = imgIndex !== -1;
                            hasContentsImg &&
                                this.contents.splice(
                                    this.contents.indexOf(
                                        this.contents.find(el => el.tag === "img")
                                    ),
                                    1
                                );
                        }
                        break;
                }
            });
        },
    });
};
