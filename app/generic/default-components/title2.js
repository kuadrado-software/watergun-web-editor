"use strict";

const genericImplementation = require("./generic-implementation");

module.exports = function title2() {
    return Object.assign(genericImplementation(title2), {
        name: "Title 2",
        template() {
            return { tag: "h2", contents: "" };
        },
        applyMutationOption: "auto",
    });
};
