"use strict";

const { getFormFields } = require("../utils");

module.exports = function genericImplementation(model) {
    /* This is the interface for defaultComponents implementation */
    return {
        name: "",
        template() {
            return { tag: "", contents: "" };
        },
        bindDataInputs(arg = {}) {
            // Default is inlineText implementation
            const { editInstance } = arg;
            return {
                tag: "div",
                class: "data-binder-inputs-container",
                contents: [
                    {
                        tag: "div",
                        class: "input-block",
                        contents: [
                            {
                                tag: "input",
                                type: "text",
                                name: "contents",
                                required: true,
                                autofocus: true,
                                placeholder: "Enter your text here",
                                defaultValue: editInstance
                                    ? editInstance.contents.replaceAll("&nbsp;", " ")
                                    : "",
                            },
                        ],
                    },
                ],
            };
        },
        mutationOptions(arg = {}) {
            return [model().bindDataInputs({ editInstance: this, ...arg })];
        },
        handleMutationSubmit(form, externalObject = undefined) {
            const applyTo = externalObject || this;
            getFormFields(form).forEach(f => {
                applyTo[f.name] = f.value;
            });
        },
        // If the type of the form fields allow to submit by pressing enter or by closing a popup, this can be set to "auto"
        applyMutationOption: "button",
    };
};
