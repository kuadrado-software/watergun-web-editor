"use strict";

const genericImplementation = require("./generic-implementation");

module.exports = function link() {
    return Object.assign(genericImplementation(link), {
        name: "Link text",
        template() {
            return { tag: "a" };
        },
        bindDataInputs(arg = {}) {
            const { editInstance = {} } = arg;
            const { href = "", contents = "" } = editInstance || {};

            return {
                tag: "div",
                class: "data-binder-inputs-container",
                contents: [
                    {
                        tag: "div",
                        class: "input-block",
                        contents: [
                            {
                                tag: "label",
                                for: "href",
                                contents: "Url",
                            },
                            {
                                tag: "input",
                                type: "url",
                                name: "href",
                                defaultValue: href,
                                placeholder: "https://www.example.com",
                                required: true,
                            },
                        ],
                    },
                    {
                        tag: "div",
                        class: "input-block",
                        contents: [
                            {
                                tag: "label",
                                for: "text",
                                contents: "Link text",
                            },
                            {
                                tag: "input",
                                type: "text",
                                name: "contents",
                                defaultValue: contents,
                                placeholder: "Example Website",
                                required: true,
                            },
                        ],
                    },
                ],
            };
        },
    });
};
