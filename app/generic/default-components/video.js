"use strict";

const StaticFileInput = require("../inputs/static-file-input");
const { getFormFields, getFileExt } = require("../utils");
const genericImplementation = require("./generic-implementation");

module.exports = function video() {
    return Object.assign(genericImplementation(video), {
        name: "Video",
        template() {
            return {
                tag: "video",
                style_rules: { maxWidth: "100%" },
                controls: true,
                contents: [{ tag: "source" }],
            };
        },
        bindDataInputs(arg = {}) {
            const { editInstance } = arg;
            return {
                tag: "div",
                class: "data-binder-inputs-container",
                contents: [
                    {
                        tag: "div",
                        class: "input-block",
                        contents: [
                            new StaticFileInput({ staticDir: "video", editInstance }).render(),
                        ],
                    },
                ],
            };
        },
        handleMutationSubmit(form, externalObject = undefined) {
            const applyTo = externalObject || this;
            // apply inputs to the audio inner source element
            const sourceEl = applyTo.contents.find(el => el.tag === "source");
            getFormFields(form).forEach(f => {
                sourceEl[f.name] = f.value;
            });
            sourceEl.type = `video/${getFileExt(sourceEl.src || "")}`;
        },
    });
};
