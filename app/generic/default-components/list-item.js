"use strict";

const genericImplementation = require("./generic-implementation");

module.exports = function listItem() {
    return Object.assign(genericImplementation(listItem), {
        name: "List item",
        template() {
            return { tag: "li" };
        },
        bindDataInputs(arg = {}) {
            const { editInstance } = arg;
            return {
                tag: "div",
                class: "data-binder-inputs-container",
                contents: "List item mutation form (styling only ?)",
            };
        },
    });
};
