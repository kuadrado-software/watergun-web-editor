"use strict";

const genericImplementation = require("./generic-implementation");

module.exports = function title1() {
    return Object.assign(genericImplementation(title1), {
        name: "Title 1",
        template() {
            return { tag: "h1", contents: "" };
        },
        applyMutationOption: "auto",
    });
};
