"use strict";

const globalActions = require("../global-actions");
const genericImplementation = require("./generic-implementation");
// WIP
module.exports = function linkBox() {
    return Object.assign(genericImplementation(linkBox), {
        name: "Link box",
        template() {
            return {
                tag: "linkBox",
                style_rules: {
                    display: "inline-block",
                },
                contents: [globalActions.getComponentsManager().defaultComponents.box().template()],
            };
        },
        bindDataInputs(arg = {}) {
            const { editInstance = {} } = arg;
            const { href = "" } = editInstance;
            return {
                tag: "div",
                class: "data-binder-inputs-container",
                contents: (arg.editInstance
                    ? [
                          {
                              tag: "div",
                              class: "box-overview",
                              style_rules: { position: "relative" },
                              contents: [
                                  {
                                      tag: "span",
                                      contents: editInstance.href,
                                      style_rules: {
                                          // TODO move css to file
                                          position: "absolute",
                                          top: "-3px",
                                          left: 0,
                                          fontStyle: "italic",
                                          fontSize: ".8em",
                                          letterSpacing: "1px",
                                      },
                                  },
                                  editInstance,
                              ],
                          },
                      ]
                    : []
                ).concat([
                    {
                        tag: "div",
                        class: "input-block",
                        contents: [
                            {
                                tag: "label",
                                for: "href",
                                contents: "Url",
                            },
                            {
                                tag: "input",
                                type: "url",
                                name: "href",
                                defaultValue: href,
                                placeholder: "https://www.example.com",
                                required: true,
                            },
                        ],
                    },
                ]),
            };
        },
    });
};
