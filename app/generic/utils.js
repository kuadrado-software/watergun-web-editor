"use strict";

function getFormFields(form) {
    const customInputsClasses = [
        "list-input",
        "static-file-input",
        "box-input",
        "element-selector",
        "download-link-input",
    ]; // TODO move this in constants ?

    return Array.from(form.querySelectorAll("textarea, select"))
        .concat(
            Array.from(form.getElementsByTagName("input")).filter(input =>
                ["text", "url", "number", "checkbox" /*...*/].includes(input.type)
            )
        )
        .concat(Array.from(form.querySelectorAll(customInputsClasses.map(c => `.${c}`).join(" ,"))))
        .filter(input => !input.reserved)
        .map(input => {
            return Object.assign(input, {
                value:
                    typeof input.value === "function"
                        ? input.value()
                        : typeof input.value === "string"
                        ? input.value.replaceAll("\n", "<br />").replaceAll("  ", "&nbsp;&nbsp;")
                        : input.value,
            });
        })
        .filter(input => !!input.value)
}

function slugify(str) {
    // thanks to https://gist.github.com/codeguy/6684588

    str = str.replace(/^\s+|\s+$/g, ""); // trim
    str = str.toLowerCase();

    // remove accents, swap ñ for n, etc
    var from = "àáäâèéëêìíïîòóöôùúüûñç·/_,:;";
    var to = "aaaaeeeeiiiioooouuuunc------";
    for (var i = 0, l = from.length; i < l; i++) {
        str = str.replace(new RegExp(from.charAt(i), "g"), to.charAt(i));
    }

    str = str
        .replace(/[^a-z0-9 -]/g, "") // remove invalid chars
        .replace(/\s+/g, "-") // collapse whitespace and replace by -
        .replace(/-+/g, "-") // collapse dashes
        .replace(/-$/, ""); // remove last dash if str ends with a dash

    return str;
}

function getFileExt(path) {
    const splitPath = path.split(".");
    return splitPath[splitPath.length - 1].toLowerCase();
}

module.exports = {
    getFormFields: getFormFields,
    slugify: slugify,
    getFileExt: getFileExt,
};
