"use strict";

class ProjectDirectory {
    constructor(name, path) {
        this.name = name;
        this.path = path;
    }
}

module.exports = ProjectDirectory;