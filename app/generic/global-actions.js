"use strict";

const { shell } = require("electron");

function placeholder(funcname) {
    console.error(new Error(`${funcname} isn't implemented`));
}

module.exports = {
    onUnsavedChanges: placeholder.bind(this, "onUnsavedChanges"),
    showPopup: placeholder.bind(this, "showPopup"),
    closePopup: placeholder.bind(this, "closePopup"),
    openFile: function (filePath) {
        shell.openPath(filePath);
    },
    showLoader: placeholder.bind(this, "showLoader"),
    hideLoader: placeholder.bind(this, "hideLoader"),
};
