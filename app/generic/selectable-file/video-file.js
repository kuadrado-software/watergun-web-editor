"use strict";

const VideoOverviewPopup = require("../../main-activity/components/popups/video-overview-popup/video-overview-popup");
const SelectableStaticFile = require("./selectable-static-file");

class VideoFile extends SelectableStaticFile {
    constructor(/*FilesTreeNode*/ pageNode) {
        super(pageNode);
        this.fileType = "video";
        this.overViewPopup = new VideoOverviewPopup({ video: this.getDisplayTemplate() });
    }

    getDisplayTemplate() {
        return {
            tag: "video",
            controls: true,
            contents: [
                {
                    tag: "source",
                    src: this.filePath,
                    type: `video/${this.getFileExt()}`,
                },
            ],
        };
    }
}

module.exports = VideoFile;
