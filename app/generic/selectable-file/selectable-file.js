"use strict";

const { editor_style_classes, editor_style_ids } = require("../../constants");
const EditableElement = require("../editable-element");
const fs = require("fs-extra");

class SelectableFile {
    constructor(rootFileContents) {
        this.id = window.performance.now(); // time in nanoseconds as a unique id
        this.formatElements(rootFileContents);
        this.state = {
            unsavedChanges: false,
        };
    }

    deleteElementAndGetParent(/*EditableElement*/ selectedElement) {
        const findElement = (rootTree = this.contents) => {
            let found;
            for (const el of rootTree.contents) {
                if (el.objectId === selectedElement.objectId) {
                    found = [el, rootTree];
                    break;
                }
            }

            if (!found) {
                for (const el of rootTree.contents) {
                    if (Array.isArray(el.contents) && el.contents.length > 0) {
                        found = findElement(el);
                    }
                    if (found) break;
                }
            }
            return found;
        };

        const [element, parent] = findElement();
        // TODO show a warning popup and pass the actual delete action as a callback
        parent.contents.splice(parent.contents.indexOf(element), 1);
        this.onChangeContents();
        return parent;
    }

    onChangeContents() {
        this.state.unsavedChanges = true;
    }

    formatElements(rootFileContents) {
        this.contents = new EditableElement(rootFileContents, this.id);
    }

    getOutput() {
        const trimKeys = ["fileFromId", "editionHandler"];
        const trimStyleClasses = Object.values(editor_style_classes).concat(
            Object.values(editor_style_ids)
        );
        const handle = (/*EditableElement*/ obj) => {
            return Object.entries(obj)
                .filter(kv => {
                    const [k, _v] = kv;
                    return !trimKeys.includes(k);
                })
                .reduce((acc, cur) => {
                    let [key, value] = cur;
                    if (key === "contents" && Array.isArray(value) && value.length > 0) {
                        value = value.filter(el => el.fileFromId === this.id).map(el => handle(el));
                    } else if (key === "class" || key === "id") {
                        value = value
                            .split(" ")
                            .filter(s => !trimStyleClasses.includes(s))
                            .join(" ");
                    }
                    return Object.assign(
                        acc,
                        value
                            ? {
                                [key]: value,
                            }
                            : {}
                    );
                }, {});
        };

        const objClone = this.contents.clone();

        return JSON.stringify(handle(objClone));
    }

    save() {
        try {
            fs.writeFileSync(this.filePath, this.getOutput());
        } catch (e) {
            throw e;
        }
        this.state.unsavedChanges = false;
    }
}

module.exports = SelectableFile;
