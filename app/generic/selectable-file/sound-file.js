"use strict";

const SelectableStaticFile = require("./selectable-static-file");

class SoundFile extends SelectableStaticFile {
    constructor(/*FilesTreeNode*/ pageNode) {
        super(pageNode);
        this.fileType = "sound";
    }

    getDisplayTemplate() {
        return {
            tag: "audio",
            controls: true,
            contents: [
                {
                    tag: "source",
                    src: this.filePath,
                    type: `audio/${this.getFileExt()}`,
                },
            ],
        };
    }
}

module.exports = SoundFile;
