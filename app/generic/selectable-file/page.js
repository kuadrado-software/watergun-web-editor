"use strict";

const SelectableFile = require("./selectable-file");

class Page extends SelectableFile {
    constructor(/*FilesTreeNode*/ pageNode) {
        const { name, contents, path } = pageNode;
        super(contents);
        this.filePath = path;
        this.name = name.replace(".json", "");
        this.fileType = "page";
    }
}

module.exports = Page;
