"use strict";

const globalActions = require("../global-actions");

class SelectableStaticFile {
    constructor(/*FilesTreeNode*/ file) {
        this.id = window.performance.now(); // time in nanoseconds as a unique id
        const { name, path } = file;
        this.filePath = path;
        this.name = name;
    }

    getFileExt() {
        const splitPath = this.filePath.split(".");
        return splitPath[splitPath.length - 1];
    }

    onFileOverviewClick(e) {
        e.preventDefault();
        this.overViewPopup && globalActions.showPopup(this.overViewPopup);
        this.openExternal && globalActions.openFile(this.filePath);
    }
}

module.exports = SelectableStaticFile;
