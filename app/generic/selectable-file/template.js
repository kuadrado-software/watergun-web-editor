"use strict";

const fs = require("fs-extra");
const { utils } = require("../files-tree-utils");
const SelectableFile = require("./selectable-file");

class Template extends SelectableFile {
    constructor(data, savedState) {
        const { modelFilePath, outputDirectory } = data;
        const tmpSplitPath = modelFilePath.split("/");
        const name = tmpSplitPath[tmpSplitPath.length - 2];
        const filePath = `${outputDirectory}/${name}.json`;

        if (!savedState) {
            try {
                super(utils.getFileContents(filePath, utils.getFileParentDirPath(filePath)));
            } catch (error) {
                console.log("Output template file is not created yet", error);
                super(
                    utils.getFileContents(modelFilePath, utils.getFileParentDirPath(modelFilePath)),
                );
                this.createOutputDestination(filePath);
            }
        } else {
            super(savedState.contents);
        }

        this.name = name;
        this.filePath = filePath;
        this.fileType = "template";
    }

    createOutputDestination(filePath) {
        fs.writeFileSync(filePath, this.getOutput());
    }

    findNodeById(id, searchTree = this.contents.contents) {
        const found = searchTree.find(node => node.id === id);
        if (!found) {
            for (const node of searchTree.filter(
                n => Array.isArray(n.contents) && n.contents.length > 0
            )) {
                this.findNodeById(id, node.contents);
                if (this.nodeFound) break;
            }
        }

        return found;
    }
}

module.exports = Template;
