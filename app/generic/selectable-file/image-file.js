"use strict";

const ImageOverviewPopup = require("../../main-activity/components/popups/image-overview-popup/image-overview-popup");
const SelectableStaticFile = require("./selectable-static-file");

class ImageFile extends SelectableStaticFile {
    constructor(/*FilesTreeNode*/ pageNode) {
        super(pageNode);
        this.fileType = "image";
        this.overViewPopup = new ImageOverviewPopup({ image: this.getDisplayTemplate() });
    }

    getDisplayTemplate() {
        return {
            tag: "img",
            src: this.filePath,
        };
    }
}

module.exports = ImageFile;
