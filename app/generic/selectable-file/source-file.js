"use strict";

const { icons_dir_path } = require("../../constants");
const SelectableStaticFile = require("./selectable-static-file");

class SourceFile extends SelectableStaticFile {
    constructor(/*FilesTreeNode*/ pageNode) {
        super(pageNode);
        this.fileType = "source";
        this.openExternal = true;
    }

    getDisplayTemplate() {
        return {
            tag: "div",
            class: "document-display-template",
            contents: [
                {
                    tag: "img",
                    src: `${icons_dir_path}/document.svg`,
                },
                {
                    tag: "div",
                    class: "filename",
                    contents: [{tag: "span", contents: this.name}],
                },
            ],
        };
    }
}

module.exports = SourceFile;
