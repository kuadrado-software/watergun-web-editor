"use strict";

const {
    DEFAULT_COMPONENTS,
    PAGE_CONTAINER_MODEL,
} = require("./default-components/default-components");

class ComponentsManager {
    constructor() {
        this.tags_map = {
            div: "box",
            section: "box",
            aside: "box",
            main: "box",
            nav: "box",
            header: "box",
            footer: "box",

            h1: "title1",
            h2: "title2",
            h3: "title3",
            h4: "title4",
            // h5: "title5",
            // h6: "title6",
            p: "paragraph",
            span: "inlineText",
            a: "link",
            button: "button",

            ul: "list",
            li: "listItem",

            img: "image",
            audio: "audio",
            source: "source",
            video: "video",
            // iframe: "iframe",
            // canvas: "canvas",

            b: "inlineText",
            i: "inlineText",
            u: "inlineText",
            strong: "inlineText",

            // form: "form",
            // table: "table",
            // th: "tableHead",
            // tr: "tableRow",
            // td: "tableData",*

            // input: "input",
            // textarea: "textarea",
            // select: "select",
            // option: "option",

            // CUSTOM TAGS
            document: "document",
            linkBox: "linkBox",
            downloadLink: "downloadLink",
        };

        this.defaultComponents = DEFAULT_COMPONENTS;
    }

    getElementGenericModel(/*EditableElement*/ element) {
        if (element.id === "page-container") {
            return PAGE_CONTAINER_MODEL();
        }
        return this.defaultComponents[this.tags_map[element.tag]]();
    }

    getTagGenericModel(tag) {
        return this.defaultComponents[this.tags_map[tag]]();
    }
}

module.exports = ComponentsManager;
