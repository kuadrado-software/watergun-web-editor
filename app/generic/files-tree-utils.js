"use strict";
const fs = require("fs-extra");

const utils = {
    getFileContents: function getFileContents(filePath, parentDirPath) {
        const fileIsJson = filePath.includes(".json");
        try {
            if (!fileIsJson) {
                var fContents = fs.readFileSync(filePath);
            } else {
                var fContents = JSON.parse(fs.readFileSync(filePath, "utf8"));
            }
        } catch (error) {
            throw error;
        }

        fileIsJson && Object.entries(fContents)
            .filter(kv => {
                const [key, val] = kv;
                return (
                    (key === "contents" || key === "style_rules") &&
                    typeof val === "string" &&
                    val.includes("<file>")
                );
            })
            .forEach(kv => {
                const [key, val] = kv;
                const path = `${parentDirPath}/${val.replace("<file>", "")}`;

                const parentPath = this.getFileParentDirPath(path);
                fContents[key] = this.getFileContents(path, parentPath);
            });

        return fContents;
    },
    getFileParentDirPath: function (filePath) {
        const split = filePath.split("/");
        split.pop();
        return split.join("/");
    },
};

class FilesTreeNode {
    constructor(data) {
        const { dirPath, name, saved } = data;

        if (!saved) {
            const path = `${dirPath}/${name}`;
            const stats = fs.statSync(path);
            const isDir = stats.isDirectory();
            this.type = isDir ? "dir" : "file";
            this.name = name;
            this.path = path;
            this.showContents = isDir ? false : undefined;
            this.parentDirPath = dirPath;
        } else {
            this.name = saved.name;
            this.path = saved.path;
            this.showContents = saved.showContents;
            this.type = saved.type;
            this.parentDirPath = saved.parentDirPath;
        }
        this.contents = this.getContents();
    }

    getContents() {
        if (this.type === "dir") return FilesTree(this.path);
        else return utils.getFileContents(this.path, this.parentDirPath);
    }
}

function FilesTree(dirPath) {
    return fs.readdirSync(dirPath).map(fileName => new FilesTreeNode({ dirPath, name: fileName }));
}

module.exports = {
    FilesTreeNode: FilesTreeNode,
    FilesTree: FilesTree,
    utils: utils,
};
