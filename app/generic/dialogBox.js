"use strict";

const { dialog } = require("electron").remote;

class DialogBox {
    constructor(
        /** options arg example
         * {
         *      properties: ["openFile", "multiSelection"...],
         *      filters: [
         *          { name: "Images", extensions: ["jpg", "bmp", "png"] }
         *      ]
         * } */
        options
    ) {
        this.options = options;
    }

    open() {
        return dialog.showOpenDialogSync(this.options);
    }
}

module.exports = DialogBox;
