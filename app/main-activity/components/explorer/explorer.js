"use strict";

const { editor_style_classes } = require("../../../constants");
const globalActions = require("../../../generic/global-actions");
const render = require("../../../generic/render");
const session = require("../../../generic/session");
const CreatePagePopup = require("./components/create-page-popup");

const { SELECTED_FILE_CLASS } = editor_style_classes;

class Explorer {
    constructor(props) {
        this.props = props;
        this.createPagePopup = new CreatePagePopup({
            onSubmit: pageData => {
                globalActions.getProjectManager().createEmptyPage(pageData);
                render.renderCycle();
            },
            onCancel: globalActions.closePopup,
        });
    }

    handleResizeSideBar(e) {
        const sidebar = document.getElementById("explorer");

        const newWidth = e.clientX > 0 ? `${e.clientX}px` : sidebar.style.width;
        sidebar.style.width = newWidth;

        session.writeGlobalLayoutState("explorer", {
            width: newWidth,
        });
    }

    renderNoProjectLoadedContents() {
        return [
            {
                tag: "div",
                contents: "No project loaded",
            },
            { tag: "br" },
            {
                tag: "button",
                class: "gui-button",
                contents: "Open a project",
                onclick: this.props.onBrowseProjectsClick,
            },
            { tag: "br" },
            { tag: "br" },
            {
                tag: "button",
                class: "gui-button",
                onclick: this.props.onCreateProjectClick,
                contents: "Create a new project",
            },
        ];
    }

    handleSelectableFileClick(file) {
        globalActions.getProjectManager().selectFile(file);
        render.renderCycle();
    }

    handleStaticDirClick(dir) {
        globalActions.getProjectManager().selectStaticDir(dir);
        // tmp
        render.renderCycle();
    }

    handleCreatePageClick() {
        globalActions.showPopup(this.createPagePopup);
    }

    renderTemplateFile() {
        const loadedProject = globalActions.getProjectManager().getLoadedProject();
        const { selectedFile } = loadedProject.state;
        const { selectedStaticDir } = loadedProject.state;
        return [
            {
                tag: "div",
                contents: "Template",
                class: "list-title",
            },
            {
                tag: "ul",
                contents: [
                    {
                        tag: "li",
                        class:
                            !selectedStaticDir && selectedFile.id === loadedProject.template.id
                                ? SELECTED_FILE_CLASS
                                : "",
                        contents:
                            loadedProject.template.name +
                            (loadedProject.template.state.unsavedChanges ? " *" : ""),
                        onclick: this.handleSelectableFileClick.bind(this, loadedProject.template),
                    },
                ],
            },
        ];
    }

    renderPagesFiles() {
        const loadedProject = globalActions.getProjectManager().getLoadedProject();
        const { selectedFile } = loadedProject.state;
        const { selectedStaticDir } = loadedProject.state;
        return [
            {
                tag: "div",
                contents: "Pages",
                class: "list-title",
            },
            {
                tag: "ul",
                contents: loadedProject.pages
                    .map(p => {
                        return {
                            tag: "li",
                            contents: p.name + (p.state.unsavedChanges ? " *" : ""),
                            onclick: this.handleSelectableFileClick.bind(this, p),
                            class:
                                !selectedStaticDir && selectedFile.id === p.id
                                    ? SELECTED_FILE_CLASS
                                    : "",
                        };
                    })
                    .concat({
                        tag: "li",
                        class: "create-new-page-btn",
                        contents: "Create a new page",
                        onclick: this.handleCreatePageClick.bind(this),
                    }),
            },
        ];
    }

    renderStaticResourcesFiles() {
        const loadedProject = globalActions.getProjectManager().getLoadedProject();
        const { selectedStaticDir = { name: "" } } = loadedProject.state;
        return [
            {
                tag: "div",
                contents: "Static",
                class: "list-title",
            },
            {
                tag: "div",
                contents: [
                    {
                        tag: "ul",
                        contents: Object.keys(loadedProject.static).map(staticDir => {
                            return {
                                tag: "li",
                                onclick: this.handleStaticDirClick.bind(
                                    this,
                                    loadedProject.static[staticDir]
                                ),
                                class:
                                    selectedStaticDir.name === staticDir ? SELECTED_FILE_CLASS : "",
                                contents: [
                                    {
                                        tag: "div",
                                        contents: [
                                            {
                                                tag: "span",
                                                contents: staticDir,
                                                style_rules: { textTransform: "capitalize" },
                                            },
                                        ],
                                    },
                                ],
                            };
                        }),
                    },
                ],
            },
        ];
    }

    renderProjectFilesTreeContents() {
        const loadedProject = globalActions.getProjectManager().getLoadedProject();
        return [
            {
                tag: "div",
                class: "loaded-project-name",
                contents: [
                    {
                        tag: "strong",
                        contents: loadedProject.name,
                    },
                ],
            },
            {
                tag: "div",
                class: "selectable-files-lists-container",
                contents: this.renderTemplateFile()
                    .concat(this.renderPagesFiles())
                    .concat(this.renderStaticResourcesFiles()),
            },
        ];
    }

    renderExplorerContents() {
        const loadedProject = globalActions.getProjectManager().getLoadedProject();

        return loadedProject
            ? this.renderProjectFilesTreeContents()
            : this.renderNoProjectLoadedContents();
    }

    render() {
        return {
            tag: "aside",
            id: "explorer",
            contents: [
                {
                    tag: "div",
                    id: "explorer-contents-container",
                    contents: this.renderExplorerContents(),
                },
                {
                    tag: "div",
                    class: "sizer",
                    ondrag: this.handleResizeSideBar.bind(this),
                },
            ],
        };
    }
}

module.exports = Explorer;
