"use strict";

const Popup = require("../../../../generic/popup");
const { slugify } = require("../../../../generic/utils");

class CreatePagePopup extends Popup {
    getEmptyPageTemplate(pageName) {
        return {
            tag: "div",
            class: "page",
            contents: [
                {
                    tag: "h1",
                    contents: pageName,
                },
            ],
        };
    }

    handleSubmit(e) {
        e.preventDefault();

        const fields = Array.from(e.target.getElementsByClassName("input-block"))
            .map(
                block =>
                    Array.from(block.getElementsByTagName("input")).filter(
                        input => input.type === "text"
                    )[0]
            )
            .reduce((acc, cur) => {
                return Object.assign(acc, {
                    [cur.name]: cur.value,
                });
            }, {});

        this.props.onSubmit(
            Object.assign(fields, {
                contents: this.getEmptyPageTemplate(fields.name),
                slug: slugify(fields.name),
            })
        );

        this.props.onCancel(); // close popup
    }

    render() {
        super.render();
        return {
            tag: "div",
            class: "create-page-popup",
            id: this.popupId,
            contents: [
                {
                    tag: "h3",
                    contents: "Create a new page",
                },
                {
                    tag: "form",
                    onsubmit: this.handleSubmit.bind(this),
                    contents: [
                        {
                            tag: "div",
                            class: "input-block",
                            contents: [
                                {
                                    tag: "label",
                                    contents: "Choose a name",
                                },
                                {
                                    tag: "input",
                                    type: "text",
                                    name: "name",
                                },
                            ],
                        },
                        {
                            tag: "div",
                            class: "buttons",
                            contents: [
                                {
                                    tag: "input",
                                    type: "submit",
                                    class: "gui-button",
                                    value: "Create",
                                },
                                {
                                    tag: "button",
                                    class: "gui-button",
                                    contents: "Cancel",
                                    onclick: this.props.onCancel,
                                },
                            ],
                        },
                    ],
                },
            ],
        };
    }
}

module.exports = CreatePagePopup;
