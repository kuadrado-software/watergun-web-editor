"use strict";

const { remote } = require("electron");
const { Menu, app } = remote;

class AppMenu {
    static init(appRef) {
        const menu = Menu.buildFromTemplate([
            {
                label: "File",
                submenu: [
                    {
                        label: "Open a project",
                        click: appRef.handleBrowseProjectsClick.bind(appRef),
                    },
                    {
                        label: "Create a new project",
                        click: appRef.handleCreateProjectClick.bind(appRef),
                    },
                    {
                        label: "Save project",
                        click: appRef.handleSaveProject.bind(appRef),
                        accelerator: "CommandOrControl+s",
                    },
                    {
                        label: "Exit",
                        click() {
                            app.quit();
                        },
                    },
                ],
            },
            {
                label: "View",
                submenu: [
                    {
                        label: "Zoom",
                        submenu: [
                            {
                                label: "zoom-in",
                                role: "zoomIn",
                            },
                            {
                                label: "zoom-out",
                                role: "zoomOut",
                            },
                            {
                                label: "reset",
                                role: "resetZoom",
                            },
                        ],
                    },
                    {
                        label: "dev tools",
                        role: "toggleDevTools",
                    },
                ],
            },
        ]);

        Menu.setApplicationMenu(menu);
    }
}

module.exports = AppMenu;
