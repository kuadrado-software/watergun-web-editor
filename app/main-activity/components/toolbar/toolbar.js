"use strict";

const globalActions = require("../../../generic/global-actions");
const render = require("../../../generic/render");
const session = require("../../../generic/session");
const PropertiesEditor = require("./components/properties-editor");

class ToolBar {
    constructor(props) {
        this.props = props;
        session.states.TOOL_BAR_STATE = session.states.TOOL_BAR_STATE || {};
        this.stateRef = session.states.TOOL_BAR_STATE;
    }

    handleResizeSideBar(e) {
        const sidebar = document.getElementById("toolbar");

        const newWidth = e.clientX > 0 ? `${window.innerWidth - e.clientX}px` : sidebar.style.width;
        sidebar.style.width = newWidth;

        session.writeGlobalLayoutState("toolbar", {
            width: newWidth,
        });
    }

    renderTools() {
        const loadedProject = globalActions.getProjectManager().getLoadedProject();

        const r = {
            tag: "div",
            class: "tools-container",
            contents: [], // TMP, TODO handle differents states
        };

        if (loadedProject) {
            r.contents.push(
                new PropertiesEditor({
                    projectManager: globalActions.getProjectManager(),
                }).render()
            );
        }

        return r;
    }

    render() {
        const loadedProject = globalActions.getProjectManager().getLoadedProject();
        return {
            tag: "aside",
            id: "toolbar",
            class: loadedProject && loadedProject.state.selectedStaticDir ? "hidden" : "",
            contents: [
                {
                    tag: "div",
                    class: "sizer",
                    ondrag: this.handleResizeSideBar.bind(this),
                },
                this.renderTools(),
            ],
        };
    }
}

module.exports = ToolBar;
