"use strict";

const globalActions = require("../../../../generic/global-actions");
const render = require("../../../../generic/render");

class PropertiesEditor {
    constructor(props) {
        this.props = props;
    }

    handleModificationSubmit(event) {
        event.preventDefault();
        this.applySubmit();
    }

    applySubmit() {
        const { selectedElement } = globalActions.getProjectManager().getLoadedProject().state;
        try {
            selectedElement.handleMutationSubmit(
                document.getElementById("selected-element-mutation-form")
            );
        } catch (error) {
            console.log(error);
            // TODO handle error properly
            return alert("Please enter a value before submit");
        }
        globalActions.onUnsavedChanges();
        globalActions.getProjectManager().getSelectedFile().onChangeContents();
        render.renderCycle();
    }

    handleDeleteElement(/*EditableElement*/ selectedElement, event) {
        event.preventDefault();
        globalActions.getProjectManager().deleteElement(selectedElement);
        globalActions.onUnsavedChanges();
        render.renderCycle();
    }

    renderDeleteButton() {
        const { selectedElement } = globalActions.getProjectManager().getLoadedProject().state;
        if (selectedElement.protected) return [];
        else {
            return [
                {
                    tag: "div",
                    class: "delete-cpt",
                    contents: [
                        {
                            tag: "div",
                            class: "section-title",
                            contents: [{ tag: "strong", contents: "Delete the component" }],
                        },
                        {
                            tag: "button",
                            class: "gui-button",
                            contents: "Delete",
                            onclick: this.handleDeleteElement.bind(this, selectedElement),
                        },
                    ],
                },
            ];
        }
    }

    renderMutationOptions() {
        const { selectedElement } = globalActions.getProjectManager().getLoadedProject().state;

        const renderApplyBtn =
            globalActions.getComponentsManager().getElementGenericModel(selectedElement)
                .applyMutationOption === "button";

        return [
            {
                tag: "div",
                class: "section-title",
                contents: [
                    {
                        tag: "strong",
                        contents: `Edit : ${selectedElement.getGenericElementType()}`,
                    },
                ],
            },
            {
                tag: "form",
                id: "selected-element-mutation-form",
                onsubmit: this.handleModificationSubmit.bind(this),
                contents: selectedElement
                    .getMutationOptions({ triggerSubmit: this.applySubmit.bind(this) })
                    .concat(
                        renderApplyBtn
                            ? [
                                  {
                                      tag: "input",
                                      type: "submit",
                                      value: "Apply",
                                      class: "gui-button",
                                  },
                              ]
                            : []
                    ),
            },
        ].concat(this.renderDeleteButton());
    }

    render() {
        return {
            tag: "div",
            id: "properties-editor",
            contents: this.renderMutationOptions(),
        };
    }
}

module.exports = PropertiesEditor;
