"use strict";

const globalActions = require("../../../generic/global-actions");
// const session = require("../../../generic/session");
// const render = require("../../../generic/render");
const CreateProjectForm = require("./components/create-project-form/create-project-form");
const Editor = require("./components/editor/editor");
const ProjectsBrowser = require("./components/projects-browser/projects-browser");
const StaticDirBrowser = require("./components/static-dir-browser/static-dir-browser");

class Workspace {
    constructor(props) {
        this.props = props;

        // session.states.WORKSPACE_STATE = session.states.WORKSPACE_STATE || {};
        // this.stateRef = session.states.WORKSPACE_STATE;

        this.renderModes = {
            welcome: this.renderWelcomeMode.bind(this),
            editProject: this.renderEditProjectMode.bind(this),
            createProject: this.renderCreateProjectMode.bind(this),
            browseProjects: this.renderBrowseProjectsMode.bind(this),
        };
    }

    renderEditProjectMode() {
        const projectManager = globalActions.getProjectManager();
        const { selectedStaticDir } = projectManager.getLoadedProject().state;
        return selectedStaticDir ? [new StaticDirBrowser().render()] : [new Editor().render()];
    }

    renderCreateProjectMode() {
        return [
            new CreateProjectForm({
                projectManager: globalActions.getProjectManager(),
                onCreateProject: this.props.onCreateProject,
                onCancelCreateProject: this.props.onCancelCreateProject,
            }).render(),
        ];
    }

    renderWelcomeMode() {
        return [
            {
                tag: "div",
                class: "welcome-board-container",
                contents: "Welcome",
            },
        ];
    }

    renderBrowseProjectsMode() {
        return [
            new ProjectsBrowser({
                onSelectProject: this.props.onSelectProject,
                projects: globalActions.getProjectManager().projectDirectories,
                onCreateProjectClick: this.props.onCreateProjectClick,
            }).render(),
        ];
    }

    render() {
        const { workspaceModes } = this.props;
        const renderMode = Object.keys(workspaceModes).find(key => workspaceModes[key] === true);

        return {
            tag: "main",
            id: "workspace-container",
            onscroll: e => (window.workspaceScrollState = e.target.scrollTop),
            contents: this.renderModes[renderMode](),
        };
    }
}

module.exports = Workspace;
