"use strict";

const fs = require("fs-extra");
const { empty_project_template, generic_templates_path } = require("../../../../../constants");
const globalActions = require("../../../../../generic/global-actions");
const render = require("../../../../../generic/render");

const formState = {
    name: "",
    choosenTemplate: "",
};

class CreateProjectForm {
    constructor(props) {
        this.props = props;
        this.state = formState;
    }

    onSubmit(e) {
        e.preventDefault();
        const form = e.target;
        const formData = {};

        const findInputs = element => {
            const inputs = Array.from(element.getElementsByTagName("input")).filter(
                e => e.type !== "submit"
            );
            inputs.forEach(input => {
                formData[input.name] = input.value;
            });

            for (const child of element.childNodes) {
                if (child.childNodes.length > 0) {
                    findInputs(child);
                }
            }
        };

        findInputs(form);

        Object.assign(formData, {
            modelTemplatePath: `${generic_templates_path}/${this.state.choosenTemplate}`,
        });

        globalActions.getProjectManager().createEmptyProject(formData);
        this.props.onCreateProject(); // this toggles the workspace to editProject mode and triggers rendering
    }

    handleTemplateClick(f, _e) {
        this.state.choosenTemplate = f;
        render.renderCycle();
    }

    handleRadioTemplateClick(e) {
        this.state.choosenTemplate = e.target.value;
        render.renderCycle();
    }

    handleInputChange(e) {
        this.state[e.target.name] = e.target.value;
    }

    getTemplatesOverviews() {
        try {
            var files = fs.readdirSync(`${empty_project_template}/${generic_templates_path}`);
        } catch (e) {
            console.log(e);
            throw e;
        }

        const render = files.map(f => {
            return {
                tag: "li",
                class: "template-list-item",
                onclick: this.handleTemplateClick.bind(this, f),
                contents: [
                    {
                        tag: "div",
                        class: "template-name-container",
                        contents: [
                            {
                                tag: "input",
                                type: "radio",
                                name: "template",
                                required: true,
                                value: f,
                                onchange: this.handleRadioTemplateClick.bind(this),
                                checked: this.state.choosenTemplate === f,
                            },
                            {
                                tag: "span",
                                contents: f.replaceAll("-", " "),
                            },
                        ],
                    },
                    {
                        tag: "div",
                        class:
                            "template-overview-container" +
                            (this.state.choosenTemplate === f ? " selected" : ""),
                        contents: [
                            JSON.parse(
                                fs.readFileSync(
                                    `${empty_project_template}/${generic_templates_path}/${f}/index.json`,
                                    "utf8"
                                )
                            ),
                        ],
                    },
                ],
            };
        });

        return render;
    }

    render() {
        return {
            tag: "div",
            id: "create-project-form",
            contents: [
                {
                    tag: "h1",
                    contents: "Create project",
                },
                {
                    tag: "form",
                    onsubmit: this.onSubmit.bind(this),
                    contents: [
                        {
                            tag: "div",
                            class: "input-block",
                            contents: [
                                {
                                    tag: "label",
                                    for: "name",
                                    contents: "Choose a name for your project",
                                },
                                {
                                    tag: "input",
                                    type: "text",
                                    name: "name",
                                    onchange: this.handleInputChange.bind(this),
                                    placeholder: "My new website",
                                    value: this.state.name || "",
                                    required: true,
                                },
                            ],
                        },
                        {
                            tag: "div",
                            class: "choose-template-block",
                            contents: [
                                { tag: "label", contents: "Choose a base template" },
                                {
                                    tag: "ul",
                                    class: "templates-list",
                                    contents: this.getTemplatesOverviews(),
                                },
                            ],
                        },
                        {
                            tag: "div",
                            class: "buttons",
                            contents: [
                                {
                                    tag: "input",
                                    type: "submit",
                                    class: "gui-button",
                                    value: "Create",
                                },
                                {
                                    tag: "button",
                                    class: "gui-button",
                                    onclick: this.props.onCancelCreateProject,
                                    contents: "Cancel",
                                },
                            ],
                        },
                    ],
                },
            ],
        };
    }
}

module.exports = CreateProjectForm;
