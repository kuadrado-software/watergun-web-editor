"use strict";

const { static_files_importation } = require("../../../../../constants");
const DialogBox = require("../../../../../generic/dialogBox");
const globalActions = require("../../../../../generic/global-actions");
const render = require("../../../../../generic/render");

class StaticDirBrowser {
    constructor(/*ProjectManager*/ props) {
        this.props = props;
        const { selectedStaticDir } = globalActions.getProjectManager().getLoadedProject().state;
        const { name, contents } = selectedStaticDir;
        this.contents = contents;
        this.name = name;
    }

    getFileName(file) {
        return file.name.replace(/\.[a-zA-Z0-9]+/, "");
    }

    handleDeleteFileClick(file, e) {
        // TODO batch delete and confirmation popup
        e.stopImmediatePropagation();
        globalActions.getProjectManager().deleteStaticFile(file);
        render.renderCycle();
    }

    handleRenameFileClick(file, e) {
        e.stopImmediatePropagation();

        const renameForm = {
            tag: "form",
            class: "rename-form",
            onsubmit: e => {
                e.preventDefault();
                e.stopImmediatePropagation();
                const filename = Array.from(e.target.getElementsByTagName("input")).find(
                    inp => inp.name === "filename"
                ).value;
                globalActions.getProjectManager().renameStaticFile(file, filename);
                render.renderCycle();
            },
            onclick: e => e.stopImmediatePropagation(),
            contents: [
                {
                    tag: "input",
                    type: "text",
                    name: "filename",
                    autofocus: true,
                    placeholder: this.getFileName(file),
                },
                {
                    tag: "input",
                    type: "submit",
                    value: "",
                    class: "gui-button spot green",
                    title: "apply",
                },
                {
                    tag: "button",
                    class: "gui-button spot red",
                    title: "cancel",
                    onclick: e => {
                        e.stopImmediatePropagation();
                        e.preventDefault();
                        render.renderCycle();
                    },
                },
            ],
        };

        const fileNameElement = e.target.parentNode.parentNode.getElementsByClassName(
            "filename"
        )[0];

        render.subRender(renameForm, fileNameElement, { mode: "override" });
    }

    handleImportFilesClick() {
        const supportedExt = static_files_importation.supported_types_by_category[this.name];
        const dialog = new DialogBox({
            properties: ["openFile", "multiSelections"],
            filters: [{ name: this.name, extensions: supportedExt }],
        });

        const filePathes = dialog.open();
        if (filePathes) this.handleImportFiles(filePathes);
    }

    handleImportFiles(pathes) {
        globalActions.showLoader("Importing files...", () => {
            try {
                globalActions.getProjectManager().importStaticFiles(pathes, this.name);
            } catch (error) {
                console.error(error);
            }
            globalActions.hideLoader();
        });
    }

    render() {
        return {
            tag: "div",
            id: "static-dir-browser",
            contents: [
                {
                    tag: "div",
                    class: "dir-header",
                    contents: [
                        {
                            tag: "h1",
                            contents: this.name,
                        },
                        {
                            tag: "button",
                            class: "gui-button",
                            contents: "Import files",
                            onclick: this.handleImportFilesClick.bind(this),
                        },
                    ],
                },
                {
                    tag: "ul",
                    class: this.name,
                    contents: this.contents.map(file => {
                        return {
                            tag: "li",
                            class: "file-display-card",
                            onclick: file.onFileOverviewClick.bind(file),
                            contents: [
                                {
                                    tag: "div",
                                    class: "file-display-template-container",
                                    contents: [file.getDisplayTemplate()],
                                },
                                {
                                    tag: "div",
                                    class: "file-options",
                                    contents: [
                                        {
                                            tag: "span", // TMP
                                            contents: "del",
                                            onclick: this.handleDeleteFileClick.bind(this, file),
                                            class: "gui-button",
                                        },
                                        {
                                            tag: "span",
                                            contents: "edit",
                                            onclick: this.handleRenameFileClick.bind(this, file),
                                            class: "gui-button",
                                        },
                                    ],
                                },
                                {
                                    tag: "div",
                                    class: "filename",
                                    contents: [{ tag: "span", contents: this.getFileName(file) }],
                                },
                            ],
                        };
                    }),
                },
            ],
        };
    }
}

module.exports = StaticDirBrowser;
