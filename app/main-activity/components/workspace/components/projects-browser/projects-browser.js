"use strict";

// const session = require("../../../../generic/session");

// const render = require("../../../../generic/render");

class ProjectsBrowser {
    constructor(props) {
        this.props = props;
        // this.stateRef = session.states.PROJECTS_BROWSER_STATE;
    }

    renderNoProjects() {
        return {
            tag: "div",
            contents: [
                {
                    tag: "strong",
                    contents: "You don't have any project yet",
                },
                this.renderCreateProjectButton(),
            ],
        };
    }

    renderCreateProjectButton() {
        return {
            tag: "div",
            title: "Create a new project",
            onclick: this.props.onCreateProjectClick,
            contents: [
                {
                    tag: "span",
                    contents: "+",
                },
            ],
        };
    }

    renderProjectsList() {
        const { projects } = this.props;

        const renderElmt = {
            tag: "ul",
            contents: projects
                .map(project => {
                    return {
                        tag: "li",
                        contents: project.name,
                        onclick: () => this.props.onSelectProject(project),
                    };
                })
                .concat({
                    tag: "li",
                    contents: [this.renderCreateProjectButton()],
                }),
        };

        return renderElmt;
    }

    render() {
        return {
            tag: "div",
            id: "projects-browser",
            contents: [
                {
                    tag: "h1",
                    contents: "My projects",
                },
                this.renderProjectsList(),
            ],
        };
    }
}

module.exports = ProjectsBrowser;
