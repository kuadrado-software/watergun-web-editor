"use strict";

const { shell } = require("electron");
const { editor_style_classes, editor_style_ids } = require("../../../../../constants");
const globalActions = require("../../../../../generic/global-actions");
const render = require("../../../../../generic/render");
const session = require("../../../../../generic/session");

const { SELECTED_ELEMENT_CLASS, UNSELECTABLE_ELEMENT_CLASS } = editor_style_classes;
const { SELECTED_FILE_RENDER_ID } = editor_style_ids;

class Editor {
    constructor(props) {
        this.props = props;

        session.states.PROJECT_EDITOR_STATE = session.states.PROJECT_EDITOR_STATE || {
            viewMode: "edit",
        };

        this.stateRef = session.states.PROJECT_EDITOR_STATE;
    }

    handleElementMouseOver(e) {
        const { classList, parentNode, childNodes } = e.target;
        const { selectedFile } = globalActions.getProjectManager().getLoadedProject().state;
        const elmtIsSelectable = e.target.fileFromId === selectedFile.id;

        if (!elmtIsSelectable) return;

        if (!classList.contains(SELECTED_ELEMENT_CLASS)) {
            classList.add("selectable-element-hover");
        }

        if (
            parentNode.id !== SELECTED_FILE_RENDER_ID &&
            !parentNode.classList.contains(SELECTED_ELEMENT_CLASS)
        ) {
            parentNode.classList.remove("selectable-element-hover");
        }

        Array.from(childNodes)
            .filter(
                n =>
                    n.nodeType === Node.ELEMENT_NODE &&
                    !n.classList.contains(SELECTED_ELEMENT_CLASS)
            )
            .forEach(child => {
                child.classList.remove("selectable-element-hover");
            });
    }

    handleHrefClick(href) {
        shell.openExternal(href);
    }

    handleElementClick(element, e) {
        e.stopImmediatePropagation();
        e.preventDefault();
        if (e.target.href) {
            // this.handleHrefClick(e.target.href);
        }
        const { selectedFile } = globalActions.getProjectManager().getLoadedProject().state;
        const elmtIsSelectable = element.fileFromId === selectedFile.id;
        elmtIsSelectable && globalActions.getProjectManager().selectElement(element);
        render.renderCycle();
    }

    handleUnselectableElementClick(element, e) {
        e.stopImmediatePropagation();
        const projManager = globalActions.getProjectManager();
        projManager.selectFileById(element.fileFromId);
        projManager.selectElement(element);
        render.renderCycle();
    }

    handleRenderModeClick(e) {
        session.writeState(this.stateRef, {
            viewMode: e.target.stateVal,
        });
    }

    prepareElementsRender(renderObj) {
        const {
            selectedElement,
            selectedFile,
        } = globalActions.getProjectManager().getLoadedProject().state;

        const handlers = {
            edit: tree => {
                for (const el of tree) {
                    const elmtIsSelectable = el.fileFromId === selectedFile.id;

                    if (elmtIsSelectable) {
                        el.onmouseover = this.handleElementMouseOver.bind(this);
                        el.onclick = this.handleElementClick.bind(this, el);
                        el.removeClass(UNSELECTABLE_ELEMENT_CLASS);
                    } else {
                        el.addClass(UNSELECTABLE_ELEMENT_CLASS);
                        el.onclick = this.handleUnselectableElementClick.bind(this, el);
                    }

                    if (el === selectedElement) {
                        // update css class of element if it matches the current selected element
                        el.addClass(SELECTED_ELEMENT_CLASS);
                    } else {
                        el.removeClass(SELECTED_ELEMENT_CLASS);
                    }

                    if (Array.isArray(el.contents) && el.contents.length > 0) {
                        handlers.edit(el.contents);
                    }
                }
            },
            render: tree => {
                for (const el of tree) {
                    delete el.onmouseover;
                    delete el.onclick;

                    if (el.class && el.class.includes(SELECTED_ELEMENT_CLASS)) {
                        el.removeClass(SELECTED_ELEMENT_CLASS);
                    }

                    if (Array.isArray(el.contents) && el.contents.length > 0) {
                        handlers.render(el.contents);
                    }
                }
            },
        };

        handlers[this.stateRef.viewMode](renderObj.contents);
    }

    renderSelectedPageContents() {
        const { loadedPage } = globalActions.getProjectManager().getLoadedProject().state;
        const { template } = globalActions.getProjectManager().getLoadedProject();

        const pageContainer = template.findNodeById("page-container");
        pageContainer.contents = [loadedPage.contents];

        return template.contents;
    }

    render() {
        const { viewMode } = this.stateRef;

        const r = {
            tag: "div",
            id: "editor",
            class: viewMode,
            contents: [
                {
                    tag: "div",
                    class: "state-bar",
                    contents: [
                        {
                            tag: "button",
                            class: "gui-button",
                            contents: "renderMode",
                            stateVal: "render",
                            onclick: this.handleRenderModeClick.bind(this),
                        },
                        {
                            tag: "button",
                            class: "gui-button",
                            contents: "editMode",
                            stateVal: "edit",
                            onclick: this.handleRenderModeClick.bind(this),
                        },
                    ],
                },
                {
                    tag: "div",
                    id: SELECTED_FILE_RENDER_ID,
                    contents: [this.renderSelectedPageContents()],
                },
            ],
        };

        this.prepareElementsRender(r.contents.find(node => node.id === SELECTED_FILE_RENDER_ID));

        return r;
    }
}

module.exports = Editor;
