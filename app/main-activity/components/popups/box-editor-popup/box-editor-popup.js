"use strict";

const BoxEditor = require("../../../../generic/box-editor/box-editor");
const Popup = require("../../../../generic/popup");

class BoxEditorPopup extends Popup {
    constructor(props) {
        super(props);
        this.applyShiftRender = true;
    }
    render() {
        super.render();
        return {
            tag: "div",
            class: "box-editor-popup",
            id: this.popupId,
            contents: [new BoxEditor(this.props).render()],
        };
    }
}

module.exports = BoxEditorPopup;
