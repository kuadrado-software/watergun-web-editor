"use strict";

const Popup = require("../../../../generic/popup");

class ImageOverviewPopup extends Popup {
    render() {
        super.render();
        const { image } = this.props;
        return {
            tag: "div",
            class: "image-overview-popup",
            id: this.popupId,
            contents: [
                {
                    tag: "div",
                    class: "image-container",
                    contents: [image],
                },
            ],
        };
    }
}

module.exports = ImageOverviewPopup;
