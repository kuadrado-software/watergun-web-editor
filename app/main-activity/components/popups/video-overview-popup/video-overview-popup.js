"use strict";

const Popup = require("../../../../generic/popup");

class VideoOverviewPopup extends Popup {
    render() {
        super.render();
        const { video } = this.props;
        return {
            tag: "div",
            class: "video-overview-popup",
            id: this.popupId,
            contents: [
                {
                    tag: "div",
                    class: "video-container",
                    contents: [video],
                },
            ],
        };
    }
}

module.exports = VideoOverviewPopup;
