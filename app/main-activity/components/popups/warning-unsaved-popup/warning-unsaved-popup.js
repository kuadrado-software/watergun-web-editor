"use strict";

const Popup = require("../../../../generic/popup");

class WarningUnsavedPopup extends Popup {
    render() {
        const { onSave, onDiscard, onCancel } = this.props;
        return {
            tag: "div",
            class: "warning-unsaved-popup",
            id: this.popupId,
            contents: [
                {
                    tag: "h3",
                    contents: "You have unsaved changes",
                },
                {
                    tag: "div",
                    class: "buttons",
                    contents: [
                        {
                            tag: "button",
                            class: "gui-button",
                            contents: "Save changes",
                            onclick: onSave,
                        },
                        {
                            tag: "button",
                            class: "gui-button",
                            contents: "Discard changes",
                            onclick: onDiscard,
                        },
                        {
                            tag: "button",
                            class: "gui-button",
                            contents: "Cancel",
                            onclick: onCancel,
                        },
                    ],
                },
            ],
        };
    }
}

module.exports = WarningUnsavedPopup;
