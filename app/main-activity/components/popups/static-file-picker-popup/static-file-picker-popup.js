"use strict";

const { static_files_importation } = require("../../../../constants");
const DialogBox = require("../../../../generic/dialogBox");
const globalActions = require("../../../../generic/global-actions");
const Popup = require("../../../../generic/popup");
const render = require("../../../../generic/render");

class StaticFilePickerPopup extends Popup {
    constructor(props) {
        super(props);
        this.state = {};
        this.setStaticDir();
    }

    setStaticDir() {
        if (this.props.staticDir !== "*") {
            this.state.staticDir = this.props.staticDir;
        } else {
            this.state.showStaticDirs = true;
        }
    }

    handleChooseStaticDir(dir) {
        this.state.staticDir = dir;
        delete this.state.showStaticDirs;
        this.refresh();
    }

    onSelectFile(file, e) {
        e.preventDefault();
        this.props.onSelectFile(file);
    }

    onFileClick(file, e) {
        e.preventDefault();
        this.state.selectedFile = file;
        render.subRender(this.render(), document.getElementById(this.popupId), {
            mode: "replace",
        });
    }

    handleImportFilesClick() {
        const { staticDir } = this.state;
        const supportedExt = static_files_importation.supported_types_by_category[staticDir];
        const dialog = new DialogBox({
            properties: ["openFile", "multiSelections"],
            filters: [{ name: staticDir, extensions: supportedExt }],
        });

        const filePathes = dialog.open();
        if (filePathes) this.handleImportFiles(filePathes, staticDir);
    }

    refresh() {
        render.subRender(this.render(), document.getElementById(this.popupId), {
            mode: "replace",
        });
    }

    handleImportFiles(pathes) {
        const { staticDir } = this.state;

        this.state.loading = true;
        this.refresh();
        this.importTimeout = setTimeout(() => {
            try {
                globalActions.getProjectManager().importStaticFiles(pathes, staticDir, false);
            } catch (error) {
                // TODO handle error
                console.error(error);
            }
            this.state.loading = false;
            this.refresh();
        }, 200);
    }

    renderImportFilesButton() {
        return {
            tag: "button",
            class: "gui-button",
            contents: "Import files",
            onclick: this.handleImportFilesClick.bind(this),
        };
    }

    renderChooseStaticDir() {
        return {
            tag: "div",
            class: "static-file-picker-popup",
            id: this.popupId,
            class: "choose-dir",
            contents: [
                {
                    tag: "h2",
                    contents: "Choose a directory",
                },
                {
                    tag: "div",
                    class: "buttons",
                    contents: Object.keys(
                        globalActions.getProjectManager().getLoadedProject().static
                    ).map(dir => {
                        return {
                            tag: "button",
                            class: "gui-button",
                            contents: dir,
                            onclick: this.handleChooseStaticDir.bind(this, dir),
                        };
                    }),
                },
            ],
        };
    }

    render() {
        super.render();
        if (this.state.showStaticDirs) {
            return this.renderChooseStaticDir();
        }
        const { staticDir } = this.state;
        const files = globalActions.getProjectManager().getLoadedProject().static[staticDir];
        const emptyDir = files.length === 0;
        return {
            tag: "div",
            id: this.popupId,
            class: "static-file-picker-popup " + staticDir,
            contents: emptyDir
                ? this.state.loading
                    ? [{ tag: "h3", contents: "Loading files..." }]
                    : [
                          {
                              tag: "h3",
                              contents: `Your ${staticDir} gallery is empty`,
                          },
                          this.renderImportFilesButton(),
                      ]
                : [
                      {
                          tag: "div",
                          class: "files-container",
                          contents: files.map(file => {
                              return {
                                  tag: "div",
                                  class:
                                      "file-overview" +
                                      (this.state.selectedFile === file ? " selected" : ""),
                                  onclick: this.onFileClick.bind(this, file),
                                  contents: [
                                      file.getDisplayTemplate(),
                                      { tag: "span", contents: file.name },
                                  ],
                              };
                          }),
                      },
                      {
                          tag: "div",
                          class: "buttons",
                          contents: [
                              this.renderImportFilesButton(),
                              {
                                  tag: "button",
                                  class:
                                      "gui-button submit-file-pick-btn" +
                                      (this.state.selectedFile ? "" : " disabled"),
                                  contents: "Choose",
                                  onclick: this.onSelectFile.bind(this, this.state.selectedFile),
                              },
                          ],
                      },
                  ],
        };
    }
}

module.exports = StaticFilePickerPopup;
