"use strict";

const fs = require("fs-extra");
const Explorer = require("./components/explorer/explorer.js");
const ToolBar = require("./components/toolbar/toolbar.js");
const Workspace = require("./components/workspace/workspace.js");
const ProjectManager = require("../generic/project-manager.js");
const { workspace_dir } = require("../constants.js");
const AppMenu = require("./components/app-menu/app-menu.js");
const session = require("../generic/session.js");
const render = require("../generic/render.js");
const globalActions = require("../generic/global-actions.js");
const saveignore = require("./saveignore.json");
const Loader = require("../generic/loader/loader.js");
const WarningUnsavedPopup = require("./components/popups/warning-unsaved-popup/warning-unsaved-popup.js");
const { remote } = require("electron");
const ComponentsManager = require("../generic/components-manager.js");

class MainActivity {
    constructor(props) {
        this.props = props;

        AppMenu.init(this);

        this.loadSavedStateOnOpenApp();

        window.onunload = this.saveStateOnCloseApp.bind(this);
        window.onbeforeunload = e => {
            const { unsavedChanges, forceClose } = this.lightStates;
            if (unsavedChanges && !forceClose) {
                e.returnValue = false; // cancel event default behavior
                this.handleWarningUnsavedChanges(() => {
                    this.lightStates.forceClose = true;
                    remote.getCurrentWindow().close();
                });
            }
        };

        session.states.MAIN_ACTIVITY_STATE = session.states.MAIN_ACTIVITY_STATE || {
            workspaceModes: {
                welcome: true,
                editProject: false,
                createProject: false,
                browseProjects: false,
            },
        };

        this.stateRef = session.states.MAIN_ACTIVITY_STATE;

        this.lightStates = {
            // those states are not saved on closing app
            unsavedChanges: false,
            forceClose: false,
            modalLayer: {
                show: false,
                popups: [],
            },
            loaderLayer: {
                show: true,
                message: "Watergun loading...",
            },
        };

        this.projectManagerReady = false;
        this.HTMLLayoutId = "main-layout";

        this.componentsManager = new ComponentsManager();
        this.initGlobalActions();
    }

    initGlobalActions() {
        globalActions.onUnsavedChanges = this.handleUnsavedChanges.bind(this);
        globalActions.closePopup = this.handleClosePopup.bind(this);
        globalActions.showPopup = this.handleShowPopup.bind(this);
        globalActions.showLoader = this.handleShowLoader.bind(this);
        globalActions.hideLoader = this.handleHideLoader.bind(this);
        globalActions.getComponentsManager = this.getComponentsManager.bind(this);
    }

    initProjectManager() {
        // This can be a heavy task regarding the amount of file in each project
        this.projectManager = new ProjectManager();
        this.projectManagerReady = true;
        this.lightStates.loaderLayer.show = false;
        globalActions.getProjectManager = this.getProjectManager.bind(this);
    }

    getProjectManager() {
        return this.projectManager;
    }

    getComponentsManager() {
        return this.componentsManager;
    }

    handleSaveProject() {
        try {
            this.projectManager.save();
        } catch (error) {
            console.log(error);
            alert("error saving");
            return;
        }
        this.lightStates.unsavedChanges = false;
        // TMP TODO handle save success or error notification
        alert("Saved !");
        render.renderCycle();
    }

    saveStateOnCloseApp() {
        const save = { ...session.states };
        const trimIgnore = root => {
            Object.keys(root).forEach(k => {
                if (saveignore.includes(k)) {
                    delete root[k];
                } else {
                    Array.isArray(root[k]) &&
                        root[k].forEach(obj => {
                            trimIgnore(obj);
                        });

                    typeof root[k] === "object" && trimIgnore(root[k]);
                }
            });
        };
        trimIgnore(save);
        fs.writeFileSync(`${workspace_dir}/save.json`, JSON.stringify(save));
    }

    loadSavedStateOnOpenApp() {
        try {
            const saved = fs.readFileSync(`${workspace_dir}/save.json`);
            session.states = JSON.parse(saved);
        } catch {
            console.log("There is no saved state to load");
            return;
        }
    }

    setworkspaceMode(modeKey) {
        const { workspaceModes } = this.stateRef;

        Object.keys(workspaceModes).forEach(mode => {
            workspaceModes[mode] = modeKey === mode ? true : false;
        });

        session.writeState(this.stateRef, {
            workspaceModes,
        });
    }

    handleUnsavedChanges() {
        this.lightStates.unsavedChanges = true;
    }

    handleWarningUnsavedChanges(proceedCb) {
        this.handleShowPopup(
            new WarningUnsavedPopup({
                onSave: () => {
                    this.handleSaveProject();
                    this.handleClosePopup();
                    proceedCb();
                },
                onDiscard: () => {
                    this.handleClosePopup();
                    proceedCb();
                },
                onCancel: this.handleClosePopup.bind(this),
            })
        );
        return;
    }

    handleCreateProjectClick() {
        if (this.lightStates.unsavedChanges) {
            this.handleWarningUnsavedChanges(() => {
                this.clearLoadedProject();
                this.setworkspaceMode("createProject");
            });
        } else {
            this.clearLoadedProject();
            this.setworkspaceMode("createProject");
        }
    }

    handleBrowseProjectsClick() {
        if (this.lightStates.unsavedChanges) {
            this.handleWarningUnsavedChanges(() => {
                this.clearLoadedProject();
                this.setworkspaceMode("browseProjects");
            });
        } else {
            this.clearLoadedProject();
            this.setworkspaceMode("browseProjects");
        }
    }

    clearLoadedProject() {
        this.projectManager.clearLoadedProject();
    }

    handeCancelCreateProject() {
        // maybe handle a previous state
        this.setworkspaceMode("welcome");
    }

    handleSelectProject(/*ProjectDirecotry*/ projectDirectory) {
        const { path, name } = projectDirectory;
        this.projectManager.loadProject({ path, name });
        this.setworkspaceMode("editProject");
    }

    handleShowPopup(popup) {
        const { modalLayer } = this.lightStates;
        modalLayer.popups.push(popup);

        if (!modalLayer.show) {
            modalLayer.show = true;
            render.subRender(this.renderModalLayer(), document.getElementById("modal-layer"), {
                mode: "replace",
            });
        } else {
            render.subRender(
                this.renderModalLayerPopup(popup),
                document.getElementById("modal-layer")
            );

            // if popup are one on top of each other, shift the last one a little after render
            popup.shiftRender();
        }
    }

    handleClosePopup(popupId) {
        const { modalLayer } = this.lightStates;
        const { popups } = modalLayer;
        popups.splice(popups.indexOf(popups.find(popup => popup.popupId === popupId)), 1);

        if (modalLayer.popups.length === 0) {
            modalLayer.show = false;
            render.subRender(this.renderModalLayer(), document.getElementById("modal-layer"), {
                mode: "replace",
            });
        } else {
            render.subRender({}, document.getElementById("modal-layer").lastChild, {
                mode: "remove",
            });
        }
    }

    handleShowLoader(withMessage, callback) {
        const { loaderLayer } = this.lightStates;
        loaderLayer.show = true;
        loaderLayer.message = withMessage;

        render.subRender(this.renderLoaderLayer(), document.getElementById("loader-layer"), {
            mode: "replace",
        });

        if (callback) {
            // The setTimeout ensures that the DOM is resolved before a potentially heavy job take on the thread.
            this.loaderCbTimeout = setTimeout(callback, 200);
        }
    }

    handleHideLoader() {
        const { loaderLayer } = this.lightStates;
        loaderLayer.show = false;
        loaderLayer.message = "";
        this.loaderCbTimeout && clearTimeout(this.loaderCbTimeout);
        render.renderCycle();
    }

    renderModalLayer() {
        const { modalLayer } = this.lightStates;
        return {
            tag: "div",
            id: "modal-layer",
            class: modalLayer.show ? "show" : "hide",
            contents: modalLayer.show
                ? modalLayer.popups.map(popup => this.renderModalLayerPopup(popup))
                : [],
        };
    }

    renderModalLayerPopup(popup) {
        return {
            tag: "div",
            class: "popup-container",
            contents: [
                {
                    tag: "div",
                    class: "popup-topbar",
                    contents: [
                        {
                            tag: "button",
                            type: "button",
                            class: "gui-button",
                            contents: "x",
                            onclick: popup.close.bind(popup),
                        },
                    ],
                },
                popup.render(),
            ],
        };
    }

    renderLoaderLayer() {
        const { loaderLayer } = this.lightStates;
        return {
            tag: "div",
            id: "loader-layer",
            class: loaderLayer.show ? "show" : "hide",
            contents: loaderLayer.show
                ? [
                      {
                          tag: "div",
                          class: "loader-container",
                          contents: [new Loader(loaderLayer.message).render()],
                      },
                  ]
                : [],
        };
    }

    renderAppLoading() {
        return {
            tag: "div",
            id: "main-layout",
            contents: [this.renderLoaderLayer()],
        };
    }

    renderApp() {
        const { workspaceModes } = this.stateRef;
        return {
            tag: "div",
            id: this.HTMLLayoutId,
            contents: [
                new Explorer({
                    onCreateProjectClick: this.handleCreateProjectClick.bind(this),
                    onBrowseProjectsClick: this.handleBrowseProjectsClick.bind(this),
                }).render(),
                new Workspace({
                    workspaceModes,
                    onSelectProject: this.handleSelectProject.bind(this),
                    onCreateProject: this.setworkspaceMode.bind(this, "editProject"),
                    onCreateProjectClick: this.handleCreateProjectClick.bind(this),
                    onCancelCreateProject: this.handeCancelCreateProject.bind(this),
                }).render(),
                new ToolBar().render(),
                this.renderModalLayer(),
                this.renderLoaderLayer(),
            ],
        };
    }

    render() {
        return this.projectManagerReady ? this.renderApp() : this.renderAppLoading();
    }
}

module.exports = MainActivity;
